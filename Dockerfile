FROM node:19-alpine as builder
WORKDIR ./
COPY package*.json ./
RUN npm install

COPY . .
RUN npm run build

FROM nginx

RUN rm -r /usr/share/nginx/html/*

RUN rm /etc/nginx/conf.d/default.conf

COPY --from=builder ./build /usr/share/nginx/html/

COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

WORKDIR /usr/share/nginx/html
COPY ./env.sh .env* ./
RUN test -e ./.env || $(touch .env && echo "API_URL=http://localhost:3001" > ./.env && echo "CLIENT_ID=undefined" >> ./.env)

RUN chmod +x env.sh

CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
