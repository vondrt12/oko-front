import axios from "axios";

// @ts-ignore
const BASE_URL =  window._env_.API_URL

export default axios.create({
    baseURL:  BASE_URL
})

export const axiosPrivate =  axios.create({
    baseURL: BASE_URL,
    headers: {'Content-Type': 'application/json' },
    responseType:'json',
    transformResponse:(r)=>r
})