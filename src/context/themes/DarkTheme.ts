import {experimental_extendTheme as extendTheme} from "@mui/material";

//Types for custom colors
declare module '@mui/material/styles/createPalette'{
    interface Palette{
        notes: Palette['primary'];
    }
    interface PaletteOptions{
        notes: PaletteOptions['primary']
    }
}

/**
 * MUI theme settings
 * @category context-themes
 */
const theme = extendTheme({
    colorSchemes:{
        light:{
            palette:{
                notes: {
                    main:'#fddebd'
                }
            }
        },
        dark:{
            palette:{
                primary: {
                    main: '#4caf50',
                },
                secondary: {
                    main: '#f57c00',
                },
                background: {
                    paper: '#0d2e49',
                    default: '#081e2f',
                },
                notes: {
                    main:'#fddebd'
                }
            },
        }
    }
})

export default theme