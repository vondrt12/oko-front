import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {
    ExaminationData,
    ExaminationGallery,
    ExaminationOverview,
    LensModel,
    PatientData,
    PatientOverview, UserInfo
} from "./dataAPI";
import {RootState} from "./Store";

/**
 * Api slice for redux store.
 *
 * Slice contains these actions:
 *  GET:
 *    * User
 *    * Patients
 *    * Patient(patientId)
 *    * PatientsExaminations(patientId)
 *    * Examination(examinationId)
 *    * ExaminationGallery(examinationId)
 *    * Lenses
 *  UPDATE:
 *    * Patient(patientData)
 *    * Examination(examinationId)
 *  ADD:
 *    * Patient(patientData)
 *    * Examination(examinationData)
 *  DELETE:
 *    * Patient
 *    * Examination
 * @category context-store
 */
export const apiSlice = createApi({
    reducerPath:'api',
    tagTypes:['Patients','Patient', 'Examination', 'Examinations', 'Lens', 'User'],
    baseQuery: fetchBaseQuery({
        //@ts-ignore
        baseUrl:window._env_.API_URL,
        prepareHeaders: (headers, {getState})=>{
            const token = (getState() as RootState).auth.userToken
            if (token) {
                headers.set('authorization', `Bearer ${token}`)
                return headers
            }
        }
    }),
    endpoints: builder =>({
        getUser:builder.query<UserInfo, void>({
           query: ()=> `/user`,
           providesTags:['User'],
        }),
        getPatients:builder.query<Array<PatientOverview>, number>({
            query:(amount:number)=> `/patients?limit=${amount}&sort=name`,
            providesTags:['Patients']
        }),
        getPatient:builder.query<PatientData, string|undefined>({
            query:(patientId?:string)=>`/patients/${patientId}`,
            providesTags:['Patient']
        }),
        addPatient:builder.mutation<PatientData, Partial<PatientData>>({
            query:(data)=>({
                url: `patients`,
                method: 'POST',
                body: data
            }),
            invalidatesTags:['Patients']
        }),
        updatePatient:builder.mutation<PatientData, Partial<PatientData> & Pick<PatientData, 'id'>>({
            query:({id, ...patch})=>({
                url:`patients/${id}`,
                method:'PATCH',
                body:patch
            }),
            invalidatesTags:['Patient', 'Patients']
        }),
        deletePatient:builder.mutation<{success:boolean, id:string}, string | undefined>({
            query(id){
                return {
                    url: `patients/${id}`,
                    method: 'DELETE'
                }
            },
            invalidatesTags:['Patient', 'Patients']
        }),
        getPatientsExaminations:builder.query<Array<ExaminationOverview>, string | undefined>({
            query:(patientId?:string)=>`/examinations?patientId=${patientId}?sort=dateCreated`,
            providesTags:['Patient', 'Examinations']
        }),
        getExamination:builder.query<ExaminationData, string|undefined>({
            query:(examinationId?:string) => `/examinations/${examinationId}`,
            providesTags:['Examination']
        }),
        getExaminationGallery:builder.query<ExaminationGallery, string | undefined>({
            query:(examinationId?:string)=> `gallery/${examinationId}`,
            providesTags:['Examination']
        }),
        addExamination:builder.mutation<ExaminationData, Partial<ExaminationData>&{patientId:string}>({
           query:({patientId, ...data})=>({
               url:`examinations`,
               method:'POST',
               body:data,
           }),
            invalidatesTags:['Examinations', 'Patient']
        }),
        updateExamination:builder.mutation<ExaminationData, Partial<ExaminationData> & Pick<ExaminationData, 'id'>>({
            query:({id, ...patch})=>({
                url:`examinations/${id}`,
                method:'PATCH',
                body:patch
            }),
            invalidatesTags:['Examination']
        }),
        deleteExamination:builder.mutation<{success:boolean, id:string}, string |undefined>({
            query(id){
                return {
                    url: `examinations/${id}`,
                    method: 'DELETE'
                }
            },
            invalidatesTags:['Examination', 'Patient']
        }),
        getLenses:builder.query<Array<LensModel>, void>({
            query:()=>`lens`,
            providesTags:['Lens']
        }),
    })
})

export const {
    useGetPatientsQuery,
    useGetPatientQuery,
    useGetExaminationQuery,
    useGetPatientsExaminationsQuery,
    useGetExaminationGalleryQuery,
    useGetLensesQuery,
    useAddPatientMutation,
    useAddExaminationMutation,
    useUpdatePatientMutation,
    useUpdateExaminationMutation,
    useDeleteExaminationMutation,
    useDeletePatientMutation,
    useGetUserQuery
} = apiSlice