import {RawDraftContentState} from "draft-js";

/**
 * Type that holds user information
 */
export interface UserInfo{
    /**
     * Name of the user
     */
    name:string,
    /**
     * Surname of the user
     */
    surname:string,
    /**
     * Database id of the user
     */
    id:string
}

/**
 * Type that holds examination overview
 */
export interface ExaminationOverview{
    /**
     * Database id of the examination
     */
    id:string,
    /**
     * Date of creating this examination
     */
    dateCreated:Date,
    /**
     * Name of user that created this examination
     */
    createdBy:string,
    /**
     * On which side is the examined eye
     */
    side: 'right' | 'left',
    /**
     * Indicates whether all required information is filled out to generate eye model
     */
    finished: boolean
}

/**
 * Type that holds patients address information
 */
interface Address{
    /**
     * Post code
     */
    postCode?:number,
    /**
     * Name of the city
     */
    city?:string,
    /**
     * Name of the street
     */
    street?:string,
    /**
     * Number of the building
     */
    number?:number
}

/**
 * Type that holds patient overview
 */
export interface PatientOverview{
    /**
     * Database id of the patient
     */
    id?:string,
    /**
     * Name of the patient
     */
    name?:string,
    /**
     * Surname of the patient
     */
    surname?:string,
    /**
     * How many examinations records patient has
     */
    examCount?:number,
    /**
     * Citizen id of the patient (in Czech republic -> birth number)
     */
    citizenId?:string
}

/**
 * Type that holds patient information
 */
export interface PatientData{
    /**
     * Database id of the patient
     */
    id?:string,
    /**
     * Name of the patient
     */
    name?:string,
    /**
     * Surname of the patient
     */
    surname?:string,
    /**
     * Email of the patient
     */
    email?:string,
    /**
     * Citizen id of the patient (in Czech republic -> birth number)
     */
    citizenId?:string,
    /**
     * Birthday of the patient
     */
    dateOfBirth?:Date,
    /**
     * Phone number of the patient
     */
    phoneNumber?:string,
    /**
     * Address of the patient
     */
    address?:Address,
    /**
     * Doctor's notes for the patient
     */
    notes?:RawDraftContentState,
}

/**
 * Type that holds information about lens
 */
interface Lens{
    /**
     * Model of the lens
     */
    lensModel?:string,
    /**
     * Model of the lenses cylinder
     */
    cylinderModel?:string,
    /**
     * Spherical Equivalent
     */
    sphericalEquivalent?:number,
    /**
     * Originally calculated axis
     */
    originallyCalculatedAxis?:number,
    /**
     * Current axis
     */
    currentAxis?:number,
    /**
     * Decentration on X axis
     */
    decentrationAxisX?:number,
    /**
     * Decentration on Y axis
     */
    decentrationAxisY?:number,
    /**
     * Tilt of the first meridian
     */
    tiltFirstMeridian?:number,
    /**
     * Tilt of the second meridian
     */
    tiltSecondMeridian?:number,
    /**
     * Tilt of the first value
     */
    tiltFirstValue?:number,
    /**
     * Tilt of the second value
     */
    tiltSecondValue?:number
}

/**
 * Type that holds Eye Parameters from examination
 */
interface ExaminationParameters{
    /**
     *
     */
    AL?:number,
    /**
     *
     */
    CCT?:number,
    /**
     * White to White
     */
    WtW?:number,
    /**
     *
     */
    pACD?:number,
    /**
     *
     */
    K1?:number,
    /**
     *
     */
    AX1?:number,
    /**
     *
     */
    K2?:number,
    /**
     * AX2 should be perpendicular to AX1
     */
    AX2?:number,
}

/**
 * Type that holds examination information
 */
export interface ExaminationData{
    /**
     * Database id of the examination
     */
    id?:string
    /**
     * On which side is the examined eye
     */
    side?: 'left' | 'right',
    /**
     * Parameter of the eye
     */
    parameters?:ExaminationParameters,
    /**
     * Date of creating the examination
     */
    dateCreated?:Date,
    /**
     * Name of the doctor that created this examination record
     */
    createdBy?:string,
    /**
     * Parameter of the lens
     */
    lens?:Lens,
    /**
     * Doctor's notes of the examination record
     */
    notes?:RawDraftContentState,
    /**
     * Indicates whether all required information is filled out to generate eye model
     */
    finished:boolean
}

/**
 * Type that holds examination image information
 */
export interface ImageData{
    /**
     * Name of the image
     */
    name:string,
    /**
     * Date of creating the image
     */
    dateCreated:Date,
    /**
     * Description of the image
     */
    description?:string,
    /**
     * Path to source of the image
     */
    path:string,
    /**
     * Resolution of the image
     */
    resolution:{width:number, height:number}
}

/**
 * Type that holds examination gallery information
 */
export interface ExaminationGallery{
    /**
     * Array with images of plane cuts
     */
    planeCuts:Array<ImageData>,
    /**
     * Array with other examination images
     */
    images:Array<ImageData>
}

/**
 * Type that holds lenses model information
 */
export interface LensModel{
    /**
     * Name of the model
     */
    name:string,
    /**
     * Array with cylinders that the model can have
     */
    cylinders:Array<{type:string}>
}

