import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "./Store";

const initialState:{recentPatientsId:Array<string | undefined>, oldest:number, max:number} = {
    recentPatientsId:[],
    oldest:0,
    max:5
}
/**
 * Redux store slice for managing recently visited patient's profiles.
 */
const recentPatientsSlice = createSlice({
    name:"recentPatients",
    initialState,
    reducers:{
        /**
         * Adds patient's id to the array if not present. If array is full (max), new id will replace the oldest one in the array.
         * @param state current state of the recent patients
         * @param action payload with patients id
         */
        addRecentPatient: (state, action:PayloadAction<string | undefined>) =>{
            if(action.payload && !state.recentPatientsId.includes(action.payload)) {
                state.recentPatientsId[state.oldest] = action.payload
                state.oldest = (state.oldest + 1) % state.max
            }
        },
        /**
         * Removes patient's id from the array. The id is only replaced by undefined value, because it keeps the oldest value valid.
         * @param state
         * @param action
         */
        removePatientFromRecent: (state, action:PayloadAction<string | undefined>) =>{
            state.recentPatientsId = state.recentPatientsId.map((value)=>{
                if(value === action.payload) return undefined;
                return value;
            })
        }
    }
})

export const {addRecentPatient, removePatientFromRecent} = recentPatientsSlice.actions;
export default recentPatientsSlice.reducer;

export const selectRecentPatients = (state:RootState) => state.recentPatients.recentPatientsId