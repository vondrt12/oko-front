import {UserInfo} from "./dataAPI";
import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit";
import axios from "../Axios";
import {RootState} from "./Store";
import {googleLogout} from "@react-oauth/google";

interface AuthState {
    loading: boolean,
    userInfo?: UserInfo,
    userToken:string | null,
    error?: string,
    success: boolean
}

const initialState:AuthState = {
    loading: false,
    success: false,
    userToken: localStorage.getItem("userToken")
}

/**
 * Action that logs user using password and email
 * @category context-store
 */
export const userLogin = createAsyncThunk(
    'auth/login',
    async ({email, password}:{email:string, password:string}) =>{
            try{
                const response = await axios.post<{ userInfo: UserInfo, userToken: string }>("/login", {email, password})
                localStorage.setItem("userToken", response.data.userToken);
                return response.data
            } catch (err) {
               throw err
            }

    }
)

/**
 * Action that logs user using Googles JWT token
 * @category context-store
 */
export const loginWithJWTToken = createAsyncThunk(
    'auth/loginWithJWTToken',
    async ({token}:{token:string}) =>{
        try{
            const response = await axios.post<{userInfo:UserInfo, userToken:string}>("/login/accessToken", {token});
            localStorage.setItem("userToken", response.data.userToken);
            return response.data;
        } catch (err) {
            throw err
        }
    }
)

/**
 * Slice for redux store used for managing user authentication
 * @category context-store
 */
const authSlice = createSlice({
    name:'auth',
    initialState,
    reducers:{
        setCredentials: (state, { payload }: PayloadAction<UserInfo>) => {
            state.userInfo = payload
        },
        logout:(state)=>{
            googleLogout();
            localStorage.removeItem("userToken")
            state.loading = false;
            state.userInfo = undefined;
            state.userToken = null;
        }

    },
    extraReducers: (builder)=>{
        builder.addCase(userLogin.pending, (state)=>{
            state.loading = true
            state.error = undefined
        })
        builder.addCase(userLogin.fulfilled, (state, {payload})=>{
            state.loading = false;
            state.userInfo = payload.userInfo;
            state.userToken = payload.userToken;
        })
        builder.addCase(userLogin.rejected, (state, action)=>{
            state.loading = false;
            state.error = action.error.code
        })
        builder.addCase(loginWithJWTToken.pending, (state)=>{
            state.loading = true
            state.error = undefined
        })
        builder.addCase(loginWithJWTToken.fulfilled, (state, {payload})=>{
            state.loading = false;
            state.userInfo = payload.userInfo;
            state.userToken = payload.userToken;
        })
        builder.addCase(loginWithJWTToken.rejected, (state, action)=>{
            state.loading = false;
            state.error = action.error.code
        })
    }
})

export const {setCredentials, logout} = authSlice.actions;

export default authSlice.reducer;

export const selectCurrentUser = (state: RootState) => state.auth.userInfo