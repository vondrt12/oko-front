import {configureStore} from "@reduxjs/toolkit";
import statusSlice from "./statusSlice";
import {apiSlice} from "./apiSlice";
import {setupListeners} from "@reduxjs/toolkit/query";
import authSlice from "./authSlice";
import recentPatientsSlice from "./recentPatientsSlice";

/**
 * Main redux store
 * @category context-store
 */
const store = configureStore({
    reducer:{
        status: statusSlice,
        auth: authSlice,
        [apiSlice.reducerPath]:apiSlice.reducer,
        recentPatients: recentPatientsSlice
    },
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware().concat(apiSlice.middleware)
})

setupListeners(store.dispatch)

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export default store;