import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "./Store";

const initialState:{state:"idle" | "error" | "success", message?:string} = {
    state:"idle"
}

/**
 * Redux store slice responsible for managing Status state of application (Error, Success, Idle)
 * @category context-store
 */
const statusSlice = createSlice({
    name:"status",
    initialState,
    reducers:{
        setError:(state, action:PayloadAction<string>)=>{
            state.state="error";
            state.message= action.payload;
        },
        setSuccess:(state,action:PayloadAction<string>)=>{
            state.state="success";
            state.message=action.payload;
        },
        setIdle:(state)=>{
            state.state ="idle";
            state.message=undefined;
        }
    }

})

export const {setError, setSuccess, setIdle} = statusSlice.actions
export const selectStatus = (state:RootState) => state.status
export default statusSlice.reducer
