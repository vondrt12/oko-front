import React from "react";
import ReactPDF, {Document, Page, StyleSheet, Text, View} from "@react-pdf/renderer";
import {ExaminationData, ExaminationGallery, PatientData} from "../context/store/dataAPI";
import Image = ReactPDF.Image;
import Font = ReactPDF.Font;

const styles = StyleSheet.create({
    page:{
        display:'flex',
        flexDirection:'column',
    },
    wrapper:{
        height:"820px",
        margin:"20px",
        border: "2px solid black"
    },
    section:{
        borderBottom:"2px solid black",
        display:'flex',
        flexDirection:"row",
        fontSize:"15px",
        flexWrap:"wrap"
    },
    patientInfo:{
        paddingTop:"10px",
        width:"50%",
        display:'flex',
        flexDirection:"row",
    },
    bold:{
        marginRight:"5px",
        fontWeight:"light",
    },
    examInfoName: {
        width: "250px",
        fontWeight:"bold",
        fontFamily:"Open Sans",
        fontSize:'12px'
    },
    examInfoWrapper:{
        display:'flex',
        flexDirection:"row",
        flexWrap:"wrap",
        marginTop:"5px",
        fontSize:'14px',
        borderBottom:"2px solid black"
    },
    image:{
        width:"auto",
        height:"30%"
    }
})

/**
 * Component for generating PDF version of medical report
 * @param {ExaminationData} [examinationData] examination specific data (lens parameters, eye parameters, ...)
 * @param {patientData} [patientData] patient specific data (name, birthday, address, ...)
 * @param {ExaminationGallery} [gallery] images of examination (right now using only plane cuts partition)
 * @category PDF
 */
const Report = ({examinationData, patientData, gallery}:{examinationData?:ExaminationData, patientData?:PatientData, gallery?:ExaminationGallery})=>{
    Font.register({
        family: 'Open Sans',
        fonts: [
            { src: 'https://cdn.jsdelivr.net/npm/open-sans-all@0.1.3/fonts/open-sans-regular.ttf' },
            { src: 'https://cdn.jsdelivr.net/npm/open-sans-all@0.1.3/fonts/open-sans-600.ttf', fontWeight: 600 }
        ]
    });

    /**
     * Template for displaying examination parameter
     * @param {string} name name of parameter
     * @param {string} [text] value of parameter
     * @component
     */
    const ExamInfo = ({name, text}:{name:string, text?:string})=>{
        return(
            <View style={styles.examInfoWrapper}>
                <Text style={styles.examInfoName}>{name}</Text>
                <Text>: {text}</Text>
            </View>
        )
    }

    const qrCodePath = ()=>{
        const element = document.getElementById("model-path") as HTMLCanvasElement | null;
        return element === null ? "" : element.toDataURL();
    }
    return(
            <Document>
                <Page size={"A4"} style={styles.page}>
                    <View style={styles.wrapper}>
                        <View style={styles.section}>
                            <Text>Patient Info:</Text>
                        </View>
                        <View style={[styles.section, {height:"80px"}]}>
                            <View style={styles.patientInfo}>
                                <Text style={styles.bold}>Name:</Text><Text>{patientData?.name} {patientData?.surname}</Text>
                            </View>
                            <View style={styles.patientInfo}>
                                <Text style={styles.bold}>Birth date:</Text><Text>{patientData?.dateOfBirth?.toString()}</Text>
                            </View>
                            <View style={styles.patientInfo}>
                                <Text style={styles.bold}>ID:</Text><Text>{patientData?.citizenId}</Text>
                            </View>
                            <View style={styles.patientInfo}>
                                <Text style={styles.bold}>E-mail:</Text><Text>{patientData?.email}</Text>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <Text>Notes:</Text>
                        </View>
                        <View style={[styles.section, {height:"80px"}]}>

                        </View>
                        <View style={{display:'flex', flex:"1 1 auto", flexDirection:"row"}}>
                            <View style={{height:"100%", width:"300px", display:"flex", flexDirection:"column", gap:2}}>
                                <ExamInfo name={"Eye"} text={examinationData?.side}/>
                                <ExamInfo name={"AL [mm]"} text={examinationData?.parameters?.AL?.toString()}/>
                                <ExamInfo name={"CCT [um]"} text={examinationData?.parameters?.CCT?.toString()}/>
                                <ExamInfo name={"WtW [mm]"} text={examinationData?.parameters?.WtW?.toString()}/>
                                <ExamInfo name={"IOL spherical Equivalent [D]"} text={examinationData?.lens?.sphericalEquivalent?.toString()}/>
                                <ExamInfo name={"IOL model"} text={`${examinationData?.lens?.lensModel} ${examinationData?.lens?.cylinderModel}`}/>
                                <ExamInfo name={"Originally calculated IOL Axis [°]"} text={examinationData?.lens?.originallyCalculatedAxis?.toString()}/>
                                <ExamInfo name={"pACD [mm]"} text={examinationData?.parameters?.pACD?.toString()}/>
                                <ExamInfo name={"K1 [D]"} text={examinationData?.parameters?.K1?.toString()}/>
                                <ExamInfo name={"AX1 [°]"} text={examinationData?.parameters?.AX1?.toString()}/>
                                <ExamInfo name={"K2 [D]"} text={examinationData?.parameters?.K2?.toString()}/>
                                <ExamInfo name={"AX2 [°]"} text={((examinationData?.parameters?.AX1? examinationData?.parameters?.AX1 : 0) + 90).toString()}/>
                                <ExamInfo name={"Current IOL Alignment [°]"} text={examinationData?.lens?.currentAxis?.toString()}/>
                                <ExamInfo name={"Tilt1 meridian [°]"} text={examinationData?.lens?.tiltFirstMeridian?.toString()}/>
                                <ExamInfo name={"Tilt1 value [°]"} text={examinationData?.lens?.tiltFirstValue?.toString()}/>
                                <ExamInfo name={"Tilt2 meridian [°]"} text={examinationData?.lens?.tiltSecondMeridian?.toString()}/>
                                <ExamInfo name={"Tilt2 value [°]"} text={examinationData?.lens?.tiltSecondValue?.toString()}/>
                                <ExamInfo name={"Decentration X [mm]"} text={examinationData?.lens?.decentrationAxisX?.toString()}/>
                                <ExamInfo name={"Decentration Y [mm]"} text={examinationData?.lens?.decentrationAxisY?.toString()}/>

                            </View>
                            <View style={{height:"100%", display:'flex', flexDirection:"column"}}>
                                {gallery &&
                                    <>
                                        <Image src={gallery.planeCuts[0].path} style={styles.image}/>
                                        <Image src={gallery.planeCuts[1].path} style={styles.image}/>
                                        <Image src={gallery.planeCuts[2].path } style={styles.image}/>
                                    </>
                                }
                            </View>
                        </View>
                    </View>
                </Page>
                <Page size={"A4"} style={[styles.page, {display:'flex', justifyContent:'center', alignItems:'center'}]}>
                    <View style={{width:"128px"}}>
                        <Image src={qrCodePath()} style={{width:"128px"}}/>
                    </View>
                </Page>
            </Document>
    )
}
export default Report