import React from 'react';
import LogInPage from "./pages/login/LogInPage";
import {BrowserRouter, Navigate, Route, Routes} from 'react-router-dom'
import PatientSelectionPage from "./pages/selectPatient/PatientSelectionPage";
import ModelPage from './pages/model/ModelPage';
import ExaminationDetailPage from "./pages/examinationDetail/ExaminationDetailPage";
import PatientNewPage from "./pages/patientNewAndUpdate/PatientNewPage";
import PatientDetailPage from './pages/patientDetail/PatientDetailPage';
import RequireAuth from "./components/auth/RequireAuth";
import ShowNavbar from "./components/ui/layout/navbar/ShowNavbar";
import ExaminationNewPage from "./pages/examinationNewAndUpdate/ExaminationNewPage";
import {Box} from "@mui/material";
import Logout from "./pages/logout/Logout";
import EditExaminationNotesPage from "./pages/notes/EditExaminationNotesPage";
import EditPatientNotesPage from "./pages/notes/EditPatientNotesPage";
import StoreAlerts from "./components/ui/StoreAlerts";
import PatientUpdatePage from "./pages/patientNewAndUpdate/PatientUpdatePage";
import ExaminationUpdatePage from "./pages/examinationNewAndUpdate/ExaminationUpdatePage";
import PersistentLogin from "./components/auth/PersistentLogin";

const App = () => {
  return(
      <Box id={"app-wrapper"} sx={theme => ({backgroundColor:theme.palette.background.default})}>
          <StoreAlerts/>
          <BrowserRouter>
            <Routes>
                <Route path={"/"} element={<Navigate to={"/patients"}/>}/>
                <Route path={"/login"} element={<LogInPage/>}/>
                <Route element={<PersistentLogin/>}>
                    <Route path={"/patients"} element={<ShowNavbar><RequireAuth/></ShowNavbar>}>
                        <Route path={""} element={<PatientSelectionPage/>}/>
                        <Route path={"create"} element={<PatientNewPage/>}/>
                        <Route path={":id"}>
                            <Route path={""} element={<PatientDetailPage/>}/>
                            <Route path={"examinations"}>
                                <Route path={":exId"} element={<ExaminationDetailPage/>}/>
                                <Route path={"create"} element={<ExaminationNewPage/>}/>
                            </Route>

                            <Route path={"edit"}>
                                <Route path={"patient"} element={<PatientUpdatePage/>}/>
                                <Route path={"notes"} element={<EditPatientNotesPage/>}/>
                            </Route>
                        </Route>
                    </Route>
                    <Route path={"/examinations"} element={<ShowNavbar><RequireAuth/></ShowNavbar>}>
                        <Route path={":id/edit/notes"} element={<EditExaminationNotesPage/>}/>
                        <Route path={":id/edit/examination"} element={<ExaminationUpdatePage/>}/>
                    </Route>
                    <Route path={"/"} element={<RequireAuth/>}>
                        <Route path={"/logout"} element={<Logout/>}/>
                        <Route path={"/model"} element={<ModelPage/>}/>
                    </Route>
                </Route>
            </Routes>
          </BrowserRouter>
      </Box>
  )
}

export default App;
