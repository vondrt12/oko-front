import {Box, Button, Container, Divider, Paper, Stack, Typography} from "@mui/material";
import React, {FC, ReactNode} from "react";
import {ExaminationOverview} from "../../../context/store/dataAPI";
import Exam from "./Exam";
import {Add} from "@mui/icons-material";
import {useNavigate} from "react-router-dom";

interface RightHalfProps{
    renderExams:(Exam:FC<ExaminationOverview>)=>ReactNode | undefined,

}

/**
 * Content for right half of the patient detail page.
 *
 * Displays list of patient's examinations
 * @param renderExams Functions that renders the list of exams
 * @component
 */
const RightHalf:FC<RightHalfProps> = ({renderExams}) =>{
    const navigate = useNavigate();
    return(
        <Container maxWidth={"md"} sx={theme=>({padding:theme.spacing(2, 0), height:1})}>
        <Paper sx={(theme)=>({
            display:'flex',
            flexDirection:'column',
            height:1,
            gap:3,
            padding:theme.spacing(2)
        })}>

            <Box sx={{display:'flex', justifyContent:'center', gap:4}}>
                <Typography variant={"h4"} color={"primary"}>
                    Examinations
                </Typography>
                <Button variant={"outlined"} color={"secondary"} onClick={()=>navigate("examinations/create")} endIcon={<Add/>}>
                    Add exam
                </Button>
            </Box>

            <Stack spacing={1} flexGrow={1} divider={<Divider/>}>
                {renderExams(Exam)}
                <Box sx={{display:'flex', justifyContent:'center'}}>
                    <Button variant={"outlined"} color={"secondary"} onClick={()=>navigate("examinations/create")}
                            sx={{borderRadius:'100%', height:'2.5rem', width:'2.5rem', minWidth:'2.5rem'}}>
                        <Add/>
                    </Button>
                </Box>
            </Stack>
        </Paper>
        </Container>
    )
}

export default RightHalf