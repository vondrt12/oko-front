import React, {FC} from "react";
import {ExaminationOverview} from "../../../context/store/dataAPI";
import {useNavigate} from "react-router-dom";
import {ArrowForward, Circle, Visibility, VisibilityOff} from "@mui/icons-material";
import Grid from "@mui/material/Unstable_Grid2";
import ListButton from "../../../components/ui/ListButton";
import {useTheme} from "@mui/material";

/**
 * Divider for Exam template information
 * @component
 */
const InfoDivider = ()=>{
    return (
        <Grid sx={(theme)=>({paddingX:theme.spacing(1), display:'flex', alignItems:'center'})}>
            <Circle sx={{fontSize:"10px"}}/>
        </Grid>
    )
}

/**
 * Template component for displaying examination overview. On click redirects to examination's detail page.
 *
 * Template displays side, date of examination creation, name of the doctor creating the examination and icon that indicates whether the examination is finished or not
 * @param dateCreated Date when was this examination created
 * @param id Id of this examination
 * @param createdBy Doctor that created the examination
 * @param side Which side is examined eye on
 * @param finished Boolean that indicates if the model can be generated from the examination
 * @component
 */
const Exam:FC<ExaminationOverview> = ({dateCreated, id, createdBy, side, finished}) =>
{
    const theme = useTheme();
    const navigate = useNavigate();
    return (
        <ListButton onClick={()=>navigate(`examinations/${id}`)}>
            <Grid container>
                <Grid xs={1} sx={{display:'flex', justifyContent:'center', alignItems:'center', color:theme.palette.primary.main}}>
                    {finished? <Visibility/> : <VisibilityOff/>}
                </Grid>
                <Grid container xs={10} sx={(theme)=>(
                    {
                        padding:theme.spacing(1, 0, 1, 3),
                    })}>
                    <Grid>
                        {dateCreated.toString()}
                    </Grid>
                    <InfoDivider/>
                    <Grid>
                        {createdBy}
                    </Grid>
                    <InfoDivider/>
                    <Grid>
                        {side}
                    </Grid>
                </Grid>
                <Grid xs={1} sx={{display:'flex', justifyContent:'center', alignItems:'center', marginLeft:'auto',
                    [theme.breakpoints.down("sm")]:{display:'none'},
                    color:theme.palette.primary.main
                }}>
                    <ArrowForward/>
                </Grid>
            </Grid>


        </ListButton>
    )
}

export default Exam