import React, {FC, useState} from "react";
import {PatientData} from "../../../context/store/dataAPI";
import {useNavigate, useParams} from "react-router-dom";
import {Box, Container, Divider, IconButton, Paper, Stack, Tooltip, Typography, useTheme} from "@mui/material";
import EditButton from "../../../components/ui/EditButton";
import NotesPaper from "../../../components/ui/NotesPaper";
import Grid from "@mui/material/Unstable_Grid2";
import {ArrowBack} from "@mui/icons-material";
import {useDeletePatientMutation} from "../../../context/store/apiSlice";
import {useAppDispatch} from "../../../hooks/storeHooks";
import {setError, setSuccess} from "../../../context/store/statusSlice";
import RemoveButton from "../../../components/ui/RemoveButton";
import DialogConfirmation from "../../../components/ui/DialogConfirmation";
import {removePatientFromRecent} from "../../../context/store/recentPatientsSlice";

/**
 * Template for displaying patient information on patient detail page.
 * @param  info contains label and value of patient information
 * @component
 */
const InfoLine = (info:{infoName:string,info?:string}) =>
{
    return (
        <Grid container>
            <Grid  xs={4} md={6} xl={6} alignItems={"center"} display={"flex"}>
                <Typography variant={"h6"} sx={(theme)=>({marginRight:theme.spacing(1), color:theme.palette.grey["400"]})}> {info.infoName} </Typography>
            </Grid>
            <Grid alignItems={"center"} display={"flex"} xs>
                <Tooltip title={info.info}>
                    <Typography variant={"subtitle1"} noWrap sx={{textOverflow:"ellipsis", overflow:"hidden"}}> {info.info}</Typography>
                </Tooltip>
            </Grid>
        </Grid>
    )
}

/**
 * Content for left half of the patient detail page.
 *
 * Displays patients name, rest of the patient info, patient's notes.
 * Also contains buttons for editing patient data and notes, deleting patient.
 * @param patientData data of the patient
 * @component
 */
const LeftHalf:FC<PatientData> = (patientData) =>{
    const {id} = useParams();
    const navigate = useNavigate();
    const theme = useTheme();
    const [deletePatient] = useDeletePatientMutation();
    const dispatch = useAppDispatch();
    const [dialogOpened, setDialogOpened] = useState<boolean>(false);

    /**
     * Function that calls delete patient mutation on delete button click
     * On success displays success alert and redirects to patient selection page.
     * On fail displays error alert
     */
    const handleDelete = async ()=>{
        try{
            await deletePatient(id).unwrap().then(()=>{
                dispatch(setSuccess("Patient profile was successfully deleted"));
                dispatch(removePatientFromRecent(id))
                navigate("/patients");
            })
        }
        catch (err){
            console.log(err)
            dispatch(setError("Patient's profile could not be deleted"))
        }
    }
    return(
        <Container maxWidth={"md"} sx={(theme)=>({
            height:1,
            display:'flex',
            flexDirection:'column',
            gap:2,
            padding:theme.spacing(2)
        })}>
            <DialogConfirmation
                title={`Do you want to delete this profile?`}
                text={<Box>To confirm the deletion write "<Box color={"primary.main"} display={"inline-block"}>{patientData.name}</Box>" in the field bellow</Box>}
                textConfirmation={patientData.name}
                handleCloseOk={()=>{
                    handleDelete();
                    setDialogOpened(false)
                }}
                handleCloseCancel={()=>{
                    setDialogOpened(false)
                }} open={dialogOpened}
            />
            <Box sx={{display:'flex'}}>
                <Typography variant={"h3"} sx={{color:theme.palette.getContrastText(theme.palette.background.default)}}>
                    <IconButton sx={{marginRight:theme.spacing(3)}} onClick={()=>navigate("/patients")}>
                        <ArrowBack sx={{fontSize:'40px'}}/>
                    </IconButton>
                    {patientData.name} {patientData.surname}
                </Typography>
                <Box sx={{marginLeft:"auto", display:'flex', alignItems:'center'}}>
                    <RemoveButton onClick={()=>setDialogOpened(true)} tooltip={"delete patient"}/>
                </Box>
            </Box>
            <Box display={"flex"}>
                <Typography color={theme.palette.getContrastText(theme.palette.background.default)} variant={"h5"}
                            marginRight={"auto"}>Info</Typography>
                <EditButton onClick={()=>navigate("edit/patient")} tooltip={"edit patient info"}/>
            </Box>
            <Paper elevation={2} sx={(theme)=>({
                padding:theme.spacing(2)
            })}>
                <Stack divider={<Divider orientation={"horizontal"} flexItem/>} spacing={2}>
                    <InfoLine infoName={"ID"} info={patientData.citizenId?.toString()}/>
                    <InfoLine infoName={"Birth date"} info={patientData.dateOfBirth?.toString()}/>
                    <InfoLine infoName={"E-mail"} info={patientData.email}/>
                    <InfoLine infoName={"Phone number"} info={patientData.phoneNumber}/>
                    <InfoLine infoName={"Address"} info={`${patientData.address?.city}, \n
                                                            ${patientData.address?.street}, ${patientData.address?.number}
                                                            ${patientData.address?.postCode}`}/>
                </Stack>
            </Paper>
            <Box display={"flex"}>
                <Typography color={theme.palette.getContrastText(theme.palette.background.default)} variant={"h5"}
                            marginRight={"auto"}>Notes</Typography>
            <EditButton onClick={()=>navigate("edit/notes")} tooltip={"edit patient notes"}/>
            </Box>
            <NotesPaper content={patientData.notes}/>
        </Container>
    )
}

export default LeftHalf