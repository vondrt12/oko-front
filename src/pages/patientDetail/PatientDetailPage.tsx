import React, {FC, useEffect} from "react";
import {useParams} from "react-router-dom";
import {ExaminationOverview} from "../../context/store/dataAPI";
import VerticalHalfSplit from "../../components/ui/layout/VerticalHalfSplit";
import {Container} from "@mui/material";
import LeftHalf from "./components/LeftHalf";
import RightHalf from "./components/RightHalf";
import {useGetPatientQuery, useGetPatientsExaminationsQuery} from "../../context/store/apiSlice";

/**
 * Page for displaying and manipulating patient data
 * @component
 */
const PatientDetailPage = () =>
{
    const {id} = useParams();
    const {data:patientData, refetch} = useGetPatientQuery(id);
    const {data:examinations} = useGetPatientsExaminationsQuery(id);

    //Refetches patient data if id in URL does not equal id of the patient data in redux store
    useEffect(()=>{
        if(id !== patientData?.id) {
            refetch();
        }
    })

    /**
     * Function that renders list of examinations
     * @param Exam Template component for displaying examination overview data
     */
    const renderExams = (Exam:FC<ExaminationOverview>) =>
    {
        return examinations?.map((value,idx)=>
        {
            return <Exam {...value} key={idx}></Exam>
        })
    }

    return (
        <Container maxWidth={"lg"} sx={{display:'flex', flex:'1 1 auto', flexDirection:'column'}}>
            <VerticalHalfSplit
                leftHalf={
                patientData && <LeftHalf
                    {...patientData}
                />}
                rightHalf={
                <RightHalf
                    renderExams={renderExams}
                />}

            />
        </Container>
    )
}

export default PatientDetailPage