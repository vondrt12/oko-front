import useRegisterWrapper from "../../hooks/useRegisterWrapper";
import {Box, Button, Paper, TextField} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import {DatePicker} from "@mui/x-date-pickers/DatePicker";
import React, {FC, useState} from "react";
import {useController, useFormContext} from "react-hook-form";
import dayjs from "dayjs";
import {useNavigate} from "react-router-dom";
import DialogConfirmation from "../../components/ui/DialogConfirmation";

/**
 * Form for creating new patient or updating existing patient. Needs to be wrapped in FormProvider component
 * @param onSubmit Function called on form submit that accepts form data
 * @component
 */
const PatientForm:FC<{onSubmit:(data:any)=>void}> = ({onSubmit})=>{
    const methods = useFormContext();
    const customRegister = useRegisterWrapper(methods.register, methods.formState);
    const controller = useController({name:"dateOfBirth", control:methods.control, defaultValue:dayjs().format("YYYY-MM-DD")});
    const [dateOfBirth, setDateOfBirth] = useState<dayjs.Dayjs | null>(dayjs());
    const emailPattern = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/;
    const phoneNumberPattern = /([+]?[(]?[0-9]{1,2,3}[)]?[-\s.]?)?[0-9]{3}[-\s.]?[0-9]{6}$/;
    const navigate = useNavigate();
    const [dialogOpened, setDialogOpened] = useState<boolean>(false);

    return(

            <Box component={"form"} onSubmit={methods.handleSubmit(onSubmit)}>
                <DialogConfirmation
                    handleCloseOk={()=>{navigate(-1)}}
                    handleCloseCancel={()=>setDialogOpened(false)}
                    open={dialogOpened}
                    title={"Are you sure you want to leave?"}
                    text={"All unsaved changes will be lost."}
                />
                <Paper elevation={10} sx={theme=>({padding:theme.spacing(2)})}>
                    <Box>
                        <Grid container spacing={2}>
                            <Grid xs={6} md={6}>
                                <TextField fullWidth label={"Name"} {...customRegister("name", undefined, {required:true})}/>
                            </Grid>
                            <Grid xs={6} md={6}>
                                <TextField fullWidth label={"Surname"} {...customRegister("surname", undefined, {required:true})} />
                            </Grid>
                            <Grid xs={6} md={6}>
                                <DatePicker label={"Date of birth"} value={dateOfBirth} onChange={(v)=>{
                                    setDateOfBirth(v);
                                    controller.field.onChange(v?.format("YYYY-MM-DD"))}}/>
                            </Grid>
                            <Grid xs={6} md={6}>
                                <TextField fullWidth label={"Birth number"} placeholder={"eg. 000302/1725"}
                                           {...customRegister("citizenId", undefined,{pattern:/[0-9]{6}\/[0-9]{4}/, required:true})}
                                />
                            </Grid>

                            <Grid xs={12} md={6}>
                                <TextField fullWidth label={"Phone number"}
                                           {...customRegister("phoneNumber", undefined, {pattern:phoneNumberPattern})}
                                           placeholder={"eg. +420650120"}/>
                            </Grid>
                            <Grid xs={12} md={6}>
                                <TextField fullWidth label={"Email"}
                                           {...customRegister("email", undefined, {pattern:emailPattern})}/>
                            </Grid>

                            <Grid xs={12} md={12}>
                                <TextField fullWidth label={"City"}
                                           {...customRegister("address.city")}/>
                            </Grid>
                            <Grid xs={12} md={6}>
                                <TextField fullWidth label={"Street"}
                                           {...customRegister("address.street")}/>
                            </Grid>
                            <Grid md={3} xs={6}>
                                <TextField fullWidth label={"Building Number"}
                                           {...customRegister("address.number", undefined, {pattern:/[0-9]*/})}/>
                            </Grid>
                            <Grid md={3} xs={6}>
                                <TextField fullWidth label={"Post Code"}
                                           {...customRegister("address.postCode", undefined, {pattern:/[0-9]*/})}/>
                            </Grid>

                        </Grid>
                    </Box>

                </Paper>
                <Box sx={theme=>({display:'flex', justifyContent:'end', marginTop:theme.spacing(2), gap:2})}>
                    <Button variant={"outlined"} type={"button"} color={"secondary"} onClick={()=>setDialogOpened(true)}>Cancel</Button>
                    <Button variant={"contained"} type={"submit"}>Submit</Button>
                </Box>
            </Box>
    )
}
export default PatientForm