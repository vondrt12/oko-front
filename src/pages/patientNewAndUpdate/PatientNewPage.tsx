import React from "react";
import {FormProvider, useForm} from "react-hook-form";
import {useNavigate} from "react-router-dom";
import {setError, setSuccess} from "../../context/store/statusSlice";
import {useAddPatientMutation} from "../../context/store/apiSlice";
import PatientForm from "./PatientForm";
import {useAppDispatch} from "../../hooks/storeHooks";
import {Container, Typography} from "@mui/material";

/**
 * Page for creating new patient
 * @component
 */
const PatientNewPage =()=>{
    const methods = useForm({mode:"all"});
    const navigate = useNavigate();
    const [addPatient] = useAddPatientMutation();
    const dispatch = useAppDispatch();

    /**
     * Function that calls add patient mutation on from submit.
     * On success displays success alert and redirects to patient detail page of newly created patient.
     * On fail displays error alert.
     * @param data form data
     */
    const onSubmit = async (data:any)=> {
        try{
            await addPatient(data).unwrap().then(payload=>{
                dispatch(setSuccess("New patient successfully created"));
                navigate(`/patients/${payload.id}`);
            })

        } catch (err){
            dispatch(setError(`Patient could not be saved: ${err}`));
        }
    }
    return(
        <FormProvider {...methods}>
            <Container maxWidth={"md"} sx={theme=>(
                {
                    display:'flex',
                    flex:'1 1 auto',
                    justifyContent:'center',
                    alignItems:'center',
                    paddingY:theme.spacing(2),
                    flexDirection:'column',
                    gap:2
                })}>
                <Typography variant={"h3"} color={"primary"}>New patient</Typography>
                <PatientForm onSubmit={onSubmit}/>
            </Container>
        </FormProvider>
    )
}

export default PatientNewPage