import {FormProvider, useForm} from "react-hook-form";
import {useNavigate, useParams} from "react-router-dom";
import {useGetPatientQuery, useUpdatePatientMutation} from "../../context/store/apiSlice";
import {useAppDispatch} from "../../hooks/storeHooks";
import {setError, setSuccess} from "../../context/store/statusSlice";
import PatientForm from "./PatientForm";
import React from "react";
import {Container, Typography} from "@mui/material";

/**
 * Page for updating existing patient's data
 * @component
 */
const PatientUpdatePage = ()=>{
    const {id} = useParams();
    const {data:patientData} = useGetPatientQuery(id);
    const methods = useForm({mode:"all", defaultValues:{...patientData}});
    const navigate = useNavigate();
    const [updatePatient] = useUpdatePatientMutation();
    const dispatch = useAppDispatch();

    /**
     * Function that calls update patient mutation on form submit.
     * On success displays success alert and redirects to updated patient detail page.
     * On fail displays error alert.
     * @param data form data
     */
    const onSubmit = async (data:any)=> {
        try{
            await updatePatient(data).unwrap().then(payload=>{
                dispatch(setSuccess("Patient info was successfully updated"));
                navigate(`/patients/${payload.id}`);
            })

        } catch (err){
            dispatch(setError(`Patient could not be saved: ${err}`));
        }
    }
    return(
        <FormProvider {...methods}>
            <Container maxWidth={"md"} sx={theme=>(
                {
                    display:'flex',
                    flex:'1 1 auto',
                    justifyContent:'center',
                    alignItems:'center',
                    paddingY:theme.spacing(2),
                    flexDirection:'column',
                    gap:2
                })}>
                <Typography variant={"h3"} color={"primary"}>Update patient</Typography>
                <PatientForm onSubmit={onSubmit}/>
            </Container>
        </FormProvider>
    )
}

export default PatientUpdatePage