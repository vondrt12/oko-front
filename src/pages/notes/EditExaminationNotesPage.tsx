import {useNavigate, useParams} from "react-router-dom";
import FullScreenNotes from "./components/FullScreenNotes";
import {useGetExaminationQuery, useUpdateExaminationMutation} from "../../context/store/apiSlice";
import {useAppDispatch} from "../../hooks/storeHooks";
import {setError, setSuccess} from "../../context/store/statusSlice";

/**
 * Page for updating existing examination's notes
 * @component
 */
const EditExaminationNotesPage = ()=>{
    const {id} = useParams();
    const {data:examination} = useGetExaminationQuery(id);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [updateExamination] = useUpdateExaminationMutation();

    /**
     * Function that calls update examination mutation on form submit.
     * On success displays success alert and redirects to previous page.
     * On fail displays fail alert
     * @param data form data
     */
    const onSubmit = async (data:any)=>{
        try{
            await updateExamination(data).unwrap().then(response=>{
                dispatch(setSuccess("Notes were successfully updated"))
                navigate(-1)
            })

        }
        catch (err){
            dispatch(setError("Notes could not be updated"))
        }
    }

    return(
        examination ? <FullScreenNotes onSubmit={onSubmit} notes={examination.notes}/> : <p>"Examination data could not be loaded"</p>
    )
}

export default EditExaminationNotesPage;