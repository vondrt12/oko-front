import {useNavigate, useParams} from "react-router-dom";
import FullScreenNotes from "./components/FullScreenNotes";
import {useGetPatientQuery, useUpdatePatientMutation} from "../../context/store/apiSlice";
import {setError, setSuccess} from "../../context/store/statusSlice";
import {useAppDispatch} from "../../hooks/storeHooks";

/**
 * Page for updating existing patient's notes
 * @ctor
 */
const EditPatientNotesPage = ()=>{
    const {id} = useParams();
    const {data:patient} = useGetPatientQuery(id);
    const navigate = useNavigate();
    const [updatePatient] = useUpdatePatientMutation();
    const dispatch = useAppDispatch();

    /**
     * Function that calls update patient mutation on form submit
     * On success displays success alert and redirects to previous page.
     * On fail displays error alert.
     * @param data form data
     */
    const onSubmit = async (data:any)=>{
        try{
            console.log(data)
            await updatePatient({...patient,...data}).unwrap().then(response=>{
                dispatch(setSuccess("Notes were successfully updated"))
                navigate(-1)
            })

        }
        catch (err){
            dispatch(setError("Notes could not be updated"))
        }
    }

    return(
        patient ? <FullScreenNotes onSubmit={onSubmit} notes={patient.notes}/> : <p>"Patient data could not be loaded"</p>
    )
}

export default EditPatientNotesPage;