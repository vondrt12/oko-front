import {FC} from "react";
import {RawDraftContentState} from "draft-js";
import {Box, Button, Container, useTheme} from "@mui/material";
import {FormProvider, useForm} from "react-hook-form";
import NotesForm from "../../../components/forms/notes/NotesForm";
import {Save} from "@mui/icons-material";
import {useNavigate} from "react-router-dom";

/**
 * Displays Notes form with submit and cancel buttons
 * @param onSubmit function to call on form submit
 * @param notes default value of EditorState
 * @component
 */
const FullScreenNotes:FC<{onSubmit:(data:any)=>void, notes?:RawDraftContentState}> = ({onSubmit, notes})=>{
    const methods = useForm({defaultValues:{notes:notes}});
    const theme = useTheme();
    const navigate = useNavigate();
    return(
        <Container maxWidth={"sm"} sx={{display:'flex', flexGrow:'1', flexDirection:"column", justifyContent:"center"}}>
            <FormProvider {...methods}>
                <form onSubmit={methods.handleSubmit(onSubmit)}>
                    <NotesForm name={"notes"}/>
                    <Box sx={{display:'flex', justifyContent:'end', gap:2, marginTop:theme.spacing(2)}}>
                        <Button variant={"outlined"} color={"secondary"} onClick={()=>navigate(-1)}>
                            Cancel
                        </Button>
                        <Button variant={"contained"} type={"submit"} endIcon={<Save/>}>
                            Submit
                        </Button>
                    </Box>
                </form>
            </FormProvider>
        </Container>
    )
}

export default FullScreenNotes