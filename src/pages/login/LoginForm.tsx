import React from "react";
import {useForm,ValidationRule} from "react-hook-form";
import {useAppDispatch} from "../../hooks/storeHooks";
import {loginWithJWTToken, userLogin} from "../../context/store/authSlice";
import {Box, Button, Stack, TextField} from "@mui/material";
import {GoogleLogin} from "@react-oauth/google";
import {setError} from "../../context/store/statusSlice";
import useRegisterWrapper from "../../hooks/useRegisterWrapper";

const LoginForm= () =>{
    const {register, handleSubmit, formState} = useForm();
    const customRegister = useRegisterWrapper(register, formState);
    const dispatch = useAppDispatch();
    const emailPattern:ValidationRule = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/

    const login = async (data:any) =>{
        const params:{email:string, password:string} = {
            email: data.email,
            password: data.password
        }
        try{
            await dispatch(userLogin(params)).unwrap()
        }catch (err){
            dispatch(setError(`Wrong email or password`))
        }
    }

    const handleCredentialResponse = async (response:any)=>{
        try{
            await dispatch(loginWithJWTToken({token:response.credential})).unwrap()
        }catch (err){
            dispatch(setError(`User could not be authorized: ${err}.`))
        }
    }
    return(
        <Box component={"form"} onSubmit={handleSubmit(login)}>
            <Stack spacing={2}>
                
                <TextField size={"small"} label={"email"} placeholder={"e.g. north@gmail.com"} type={"email"} {...customRegister("email", undefined, {required:true, pattern:emailPattern})}/>
                <TextField size={"small"} label={"password"} type={"password"} {...customRegister("password", undefined, {required:true})}/>
                <Box sx={{display:'flex', gap:2, justifyContent:'end'}}>
                    <GoogleLogin onSuccess={handleCredentialResponse} />
                    <Button variant={"contained"} type="submit">
                        Log in
                    </Button>
                </Box>
            </Stack>
        </Box>
    )
}

export default LoginForm