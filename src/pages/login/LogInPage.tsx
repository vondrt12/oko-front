import React, {FC, useEffect} from "react";
import LoginForm from "./LoginForm";
import {useNavigate} from "react-router-dom";
import {useAppSelector} from "../../hooks/storeHooks";
import {selectCurrentUser} from "../../context/store/authSlice";
import {Container, Stack, Typography} from "@mui/material";

const LogInPage: FC = () => {
    const navigate = useNavigate();
    const userInfo = useAppSelector(selectCurrentUser)
    useEffect(()=>{
        if(userInfo){
            navigate("/patients")
        }
    }, [navigate, userInfo])
    return (
        <Container maxWidth={"sm"} sx={{display:"flex", flexDirection:"column", justifyContent:"center", flex:"1 1 auto"}}>
            <Stack spacing={2}>
                <Typography variant={"h2"} color={"text.primary"} textAlign={"center"}>
                    Sign in
                </Typography>
                <LoginForm/>
            </Stack>

        </Container>
    )
}

export default LogInPage