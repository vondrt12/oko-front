import React, {FC, useEffect} from "react";
import {useParams} from "react-router-dom";
import VerticalHalfSplit from "../../components/ui/layout/VerticalHalfSplit";
import LeftHalf from "./components/LeftHalf";
import RightHalf from "./components/RightHalf";
import {useGetExaminationGalleryQuery, useGetExaminationQuery, useGetPatientQuery} from "../../context/store/apiSlice";

/**
 * Page with examination detail.
 * @category pages
 */
const ExaminationDetailPage:FC = () =>{
    const {id,exId} = useParams();
    const {data:examination, ...examinationMethods} = useGetExaminationQuery(exId);
    const {data:patient}= useGetPatientQuery(id)
    const {data:gallery, ...galleryMethods} = useGetExaminationGalleryQuery(exId);

    //Refetches examination data if id of examination does not equal id saved in the redux store
    useEffect(()=>{
        if(exId !== examination?.id){
            examinationMethods.refetch();
            galleryMethods.refetch();
        }
    })
    return(
            <VerticalHalfSplit
                leftHalf={examination && patient && gallery && <LeftHalf patient={patient} gallery={gallery} examination={examination}/>}
                rightHalf={gallery && <RightHalf {...gallery}/>}
            />
    )
}

export default ExaminationDetailPage