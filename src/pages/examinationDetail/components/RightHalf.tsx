import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box,
    Button,
    CardActionArea,
    CardMedia,
    Container,
    Modal,
    Paper,
    Typography,
    useTheme,
} from "@mui/material";
import React, {FC, ReactNode, useState} from "react";
import InnerScrollWrapper from "../../../components/ui/layout/InnerScrollWrapper";
import {ExaminationGallery, ImageData} from "../../../context/store/dataAPI";
import {Col, Image} from "react-bootstrap";
import {ExpandMore} from "@mui/icons-material";

/**
 * Template component for gallery section image
 * @param {string} name name of the image
 * @param {React.MouseEventHandler<HTMLElement>} [onClick] function that triggers the image is clicked
 * @param {string} source source of the image
 * @component
 */
const GalleryImage = ({name, onClick, source}:{name:string, onClick?: React.MouseEventHandler<HTMLElement>, source:string}) =>{
    const theme = useTheme();
    return (
        <Col md={4} xs={6} lg={6} xl={4} style={{padding:theme.spacing(0.5), display:'flex', flexDirection:'column'}}>
            <CardActionArea onClick={onClick} sx={{borderTopLeftRadius:"5px", borderTopRightRadius:"5px"}}>
                <CardMedia
                    component={"img"}
                    style={{maxHeight:"100%", maxWidth:"100%", minWidth:"100%", height:"auto",borderTopLeftRadius:"5px", borderTopRightRadius:"5px"}}
                    image={source}

                />
            </CardActionArea>
            <Button sx={{
                borderTopRightRadius:"0px",
                borderTopLeftRadius:"0px",
            }} variant={"contained"} color={"info"} onClick={onClick}>
                {name}
            </Button>
        </Col>

    )
}

/**
 * Template component for gallery section
 * @param {string} title Title of the section
 * @param {ReactNode} [children] Content of the section
 * @component
 */
const ImageSectionWrapper= ({title, children}:{title:string, children?:ReactNode}) =>{
    return(
        <Accordion elevation={10} defaultExpanded={true} sx={(theme)=>({'&:before':{backgroundColor:theme.palette.primary.dark}})}>
            <AccordionSummary sx={(theme)=>({backgroundColor:theme.palette.primary.main, color:theme.palette.primary.contrastText})}
                              expandIcon={<ExpandMore sx={(theme)=>({color:theme.palette.primary.contrastText})}/>}
                              aria-controls={`${title}-control`}
                              id={`${title}-header`}>
                    <Typography>{title}</Typography>
            </AccordionSummary>
            <AccordionDetails id={`${title}-control`}>
                <Col className={"flex-wrap d-flex"} md={12} xs={12} xl={12}>
                    {children}
                </Col>
            </AccordionDetails>

        </Accordion>
    )
}

/**
 * Content for right half of examination detail page.
 * Displays examination's gallery.
 * @param {ExaminationGallery} gallery examination's gallery
 * @component
 */
const RightHalf:FC<ExaminationGallery> = (gallery) =>{
    const theme = useTheme();
    const [open, setOpen] = useState<boolean>(false);
    const [modalImage, setModalImage] = useState<ImageData | undefined>(undefined);

    /**
     * Handles opening the modal with clicked image.
     * @param image image that was clicked
     */
    const handleOpenModal = (image:ImageData) =>{
        setModalImage(image);
        setOpen(true);
    }

    //Renders plane cuts
    const renderPlanes = ()=>{
        return gallery.planeCuts.map((plane, index)=>{
            return <GalleryImage source={plane.path} key={index} name={plane.name} onClick={()=>handleOpenModal(plane)}/>
        })
    }

    //Renders other images
    const renderCustomImages = () =>{
        return gallery.images.map((image, index)=>{
            return <GalleryImage source={image.path} key={index} name={image.name} onClick={()=>handleOpenModal(image)}/>
        })
    }
    return(
        <Container maxWidth={"md"} sx={theme=>({padding:theme.spacing(2, 0), height:1})}>
            <Paper sx={(theme)=>({
                display:'flex',
                flexDirection:'column',
                height:1,
                gap:2,
                padding:theme.spacing(1)
            })}>
                <Modal keepMounted open={open} onClose={()=>setOpen(false)} aria-labelledby={"image-name"}>
                    <Box sx={{position:'absolute' as 'absolute', top:'50%', left:'50%',transform: 'translate(-50%, -50%)'}}>
                        <Typography variant={"h3"} id={"image-name"} textAlign={"center"} color={theme.palette.common.white}>{modalImage?.name}</Typography>
                        <Image src={modalImage?.path}/>
                    </Box>

                </Modal>

                <InnerScrollWrapper minHeight={"400px"}>
                        <ImageSectionWrapper title={"Planes"}>
                            {renderPlanes()}
                        </ImageSectionWrapper>
                        <ImageSectionWrapper title={"Custom images"}>
                            {renderCustomImages()}
                        </ImageSectionWrapper>
                </InnerScrollWrapper>
            </Paper>
        </Container>
    )
}

export default RightHalf