import {Box, Button, Container, Divider, IconButton, Paper, Stack, Typography, useTheme} from "@mui/material";
import {ArrowBack, PictureAsPdf, Visibility} from "@mui/icons-material";
import EditButton from "../../../components/ui/EditButton";
import NotesPaper from "../../../components/ui/NotesPaper";
import React, {FC, PropsWithChildren, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {ExaminationData, ExaminationGallery, PatientData} from "../../../context/store/dataAPI";
import Grid from "@mui/material/Unstable_Grid2";
import RemoveButton from "../../../components/ui/RemoveButton";
import {setError, setSuccess} from "../../../context/store/statusSlice";
import {useDeleteExaminationMutation} from "../../../context/store/apiSlice";
import {useAppDispatch} from "../../../hooks/storeHooks";
import DialogConfirmation from "../../../components/ui/DialogConfirmation";
import ReactPDF from "@react-pdf/renderer";
import Report from "../../../assets/Report";
import {saveAs} from 'file-saver'
import pdf = ReactPDF.pdf;
import QRCode from "qrcode.react";

interface LeftHalfProps{
    patient:PatientData,
    gallery:ExaminationGallery,
    examination:ExaminationData
}

interface InfoProps{
    infoName?:string,
    info?:string
}

/**
 * Template Component for displaying eye parameter info
 * @param {string} [info] parameter value
 * @param {string} [infoName] parameter name
 * @category examination detail page
 */
const EyeParameter:FC<InfoProps> = ({info, infoName}) =>{
    return(
        <Grid container xs={6}>
            <Grid>
                <Typography variant={"subtitle1"} sx={(theme)=>({marginRight:theme.spacing(1), color:theme.palette.grey["400"]})}> {infoName}: </Typography>
            </Grid>
            <Grid display={'flex'} alignItems={'center'}>
                <Typography variant={"subtitle2"} >{info}</Typography>
            </Grid>
        </Grid>
    )
}

/**
 * Template Component for displaying lens parameter info
 * @param {string} [info] parameter value
 * @param {string} [infoName] parameter name
 */
const LensParameter:FC<InfoProps> = ({info, infoName}) =>{
    return(
        <Grid container>
            <Grid>
                <Typography variant={"subtitle1"} sx={(theme)=>({marginRight:theme.spacing(1), color:theme.palette.grey["400"]})}> {infoName}: </Typography>
            </Grid>
            <Grid display={'flex'} alignItems={'center'}>
                <Typography variant={"subtitle2"} >{info}</Typography>
            </Grid>
        </Grid>
    )
}

/**
 * Wrapper for lens and eye info
 * @param {ReactNode} [children] content to be wrapped
 */
const InfoWrapper = ({children}:PropsWithChildren)=>{
    return(
        <Paper elevation={2} sx={(theme)=>({
            padding:theme.spacing(2)
        })}>
            <Stack divider={<Divider orientation={"horizontal"} flexItem/>}>
                {children}
            </Stack>
        </Paper>
    )
}

/**
 * Content for left half of the examination detail page.
 *
 * Displays from top to bottom: Name of the patient, table with examination parameters, examination's notes.
 * There are also buttons for: Editing examination parameters and notes, editing notes, deleting examination, generating model, generating and downloading pdf file with examination report.
 *
 * @param {PatientData} patient information about patient
 * @param {ExaminationData} examination information about examination
 * @param {ExaminationGallery} gallery examination's gallery
 */
const LeftHalf:FC<LeftHalfProps> = ({patient, examination, gallery}) =>{
    const { exId, id} = useParams();
    const {name, surname} = patient;
    const theme = useTheme();
    const navigate = useNavigate();
    const [deleteExamination] = useDeleteExaminationMutation();
    const dispatch = useAppDispatch();
    const [dialogOpened, setDialogOpened] = useState<boolean>(false);

    /**
     * Calculate AX2 parameter from AX1 parameter
     * @param {number} AX1 eye parameter
     */
    const calcAX2 = (AX1:number = 0) =>{
        return AX1 + 90;
    }

    /**
     * Handles examination deletion.
     * On success displays success alert and redirects to the patient detail page
     * On fail displays error alert
     */
    const handleDelete = async ()=>{
        try{
            await deleteExamination(exId).unwrap().then(()=>{
                dispatch(setSuccess("Examination was successfully deleted"));
                navigate(`/patients/${id}`);
            })
        }
        catch (err){
            console.log(err)
            dispatch(setError("Examination could not be deleted"))
        }
    }
    /**
     * Handles generating and downloading report
     * On success displays success alert and downloads generated report
     * On fail displays error alert
     */
    const handleDownloadReport = async ()=>{
        dispatch(setSuccess("Generating report"));
        const asPdf = pdf();
        asPdf.updateContainer(<Report patientData={patient} examinationData={examination} gallery={gallery}/>);
        await asPdf.toBlob().then(b=>{
            dispatch(setSuccess("Report was successfully generated"));
            saveAs(b, "report.pdf")
        }).catch((err)=>{
            dispatch(setError("Report could not be generated"))
        })
    }
    /**
     * Generates qr code with path to the model. Rendered only so it can be used in pdf creation.
     */
    const GenerateQRCode = () =>{
        return(
            <Box sx={{display:'none'}}>
                <QRCode
                    id={"model-path"}
                    value={`/patients/${patient?.id}/examinations/${examination?.id}/model`}
                    size={128}
                    level={'H'}
                />
            </Box>

        )
    }

    const renderEyeInfo = ()=>{
        return(
            <>
                <Typography variant={"h5"} sx={(theme)=>({color:theme.palette.getContrastText(theme.palette.background.default)})}>Eye parameters</Typography>
                <InfoWrapper>
                    <Grid container>
                        <EyeParameter infoName={"AL"} info={examination.parameters?.AL?.toString()}/>
                        <EyeParameter infoName={"CCT"} info={examination.parameters?.CCT?.toString()}/>
                    </Grid>
                    <Grid container>
                        <EyeParameter infoName={"pACD"} info={examination.parameters?.pACD?.toString()}/>
                        <EyeParameter infoName={"WtW"} info={examination.parameters?.WtW?.toString()}/>
                    </Grid>
                    <Grid container>
                        <EyeParameter infoName={"K1"} info={examination.parameters?.K1?.toString()}/>
                        <EyeParameter infoName={"K2"} info={examination.parameters?.K2?.toString()}/>
                    </Grid>
                    <Grid container>
                        <EyeParameter infoName={"AX1"} info={examination.parameters?.AX1?.toString()}/>
                        <EyeParameter infoName={"AX2"} info={calcAX2(examination.parameters?.AX1).toString()}/>
                    </Grid>
                </InfoWrapper>
            </>

            )

    }
    const renderLensInfo = ()=>{
        return(
            <>
                <Typography variant={"h5"} sx={(theme)=>({color:theme.palette.getContrastText(theme.palette.background.default)})}>Lens parameters</Typography>
                <InfoWrapper>
                    <LensParameter infoName={"Lens model"} info={`${examination.lens?.lensModel} - ${examination.lens?.cylinderModel}`}/>
                    <LensParameter infoName={"spherical Equivalent"} info={examination.lens?.sphericalEquivalent?.toString()}/>
                    <LensParameter infoName={"originally calculated axis"} info={examination.lens?.originallyCalculatedAxis?.toString()}/>
                    <LensParameter infoName={"currentAxis"} info={examination.lens?.currentAxis?.toString()}/>
                    <LensParameter infoName={"decentration on X axis"} info={examination.lens?.decentrationAxisX?.toString()}/>
                    <LensParameter infoName={"decentration on Y axis"} info={examination.lens?.decentrationAxisY?.toString()}/>
                    <LensParameter infoName={"tilt of meridian 1"} info={examination.lens?.tiltFirstMeridian?.toString()}/>
                    <LensParameter infoName={"tilt of meridian 2"} info={examination.lens?.tiltSecondMeridian?.toString()}/>
                    <LensParameter infoName={"tilt of meridian 1 value"} info={examination.lens?.tiltFirstValue?.toString()}/>
                    <LensParameter infoName={"tilt of meridian 2 value"} info={examination.lens?.tiltSecondValue?.toString()}/>
                </InfoWrapper>
            </>

        )

    }
    return(
        <Container maxWidth={"md"} sx={(theme)=>({
            height:1,
            display:'flex',
            flexDirection:'column',
            padding:theme.spacing(2)
        })}>
            <DialogConfirmation
                title={`Do you want to delete this examinations?`}
                text={<Box>To confirm the deletion write "<Box color={"primary.main"} display={"inline-block"}>{patient.name}</Box>" in the field bellow</Box>}
                textConfirmation={patient.name}
                handleCloseOk={()=>{
                    handleDelete();
                    setDialogOpened(false)
                }}
                handleCloseCancel={()=>{
                    setDialogOpened(false)
                }} open={dialogOpened}
            />
            <Box sx={{display:'flex'}}>
                <Typography variant={"h3"} sx={{color:theme.palette.getContrastText(theme.palette.background.default)}}>
                    <IconButton sx={{marginRight:theme.spacing(3)}} onClick={()=>navigate(`/patients/${id}`)}>
                        <ArrowBack sx={{fontSize:'40px'}}/>
                    </IconButton>
                    {name} {surname}
                </Typography>
                <Box sx={{marginLeft:"auto", display:'flex', alignItems:'center'}}>
                    <RemoveButton onClick={()=>setDialogOpened(true)} tooltip={"delete examination"}/>
                </Box>
            </Box>
            <EditButton onClick={()=>navigate(`/examinations/${exId}/edit/examination`)} tooltip={"edit patient info"}/>
            <Grid container spacing={1}>
                <Grid xs={12} md={6}>
                    {renderLensInfo()}
                </Grid>
                <Grid xs={12} md={6}>
                    {renderEyeInfo()}
                </Grid>
            </Grid>
            <Box display={"flex"} marginTop={theme.spacing(1)}>
                <Typography color={theme.palette.getContrastText(theme.palette.background.default)} variant={"h5"}
                            marginRight={"auto"}>Notes</Typography>
                <EditButton onClick={()=>navigate(`/examinations/${exId}/edit/notes`)} tooltip={"edit patient notes"}/>
            </Box>
            <NotesPaper content={examination.notes}/>
            <Box display={'flex'} justifyContent={'end'} marginTop={theme.spacing(1)} gap={2}>
                <GenerateQRCode/>
                <Button variant={"contained"} endIcon={<Visibility/>} disabled={!examination.finished} onClick={()=>navigate("/model")}>Generate model</Button>
                <Button variant={"contained"} endIcon={<PictureAsPdf/>} onClick={()=>handleDownloadReport()}>Generate report</Button>
            </Box>
        </Container>
    )
}

export default LeftHalf