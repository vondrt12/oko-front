import {Box, Button, useTheme} from "@mui/material";
import {useNavigate} from "react-router-dom";
import NotesForm from "../../../components/forms/notes/NotesForm";
import DialogConfirmation from "../../../components/ui/DialogConfirmation";
import React, {useState} from "react";

/**
 * Content of right half of the examination form.
 *
 */
const RightHalf = ()=>{
    const theme = useTheme();
    const navigate = useNavigate();
    const [dialogOpened, setDialogOpened] = useState<boolean>(false);
    return(
        <Box sx={{display:'flex', flexDirection:'column-reverse', height:1, paddingY:theme.spacing(2), gap:2, [theme.breakpoints.up("md")]:{paddingLeft:theme.spacing(2)}}}>
            <DialogConfirmation
                handleCloseOk={()=>{navigate(-1)}}
                handleCloseCancel={()=>setDialogOpened(false)}
                open={dialogOpened}
                title={"Are you sure you want to leave?"}
                text={"All unsaved changes will be lost."}
            />
            <Box sx={{display:'flex', width:1, justifyContent:'end', gap:2}}>
                <Button type={"button"} variant={"contained"} color={"secondary"} onClick={()=>setDialogOpened(true)}>
                    Cancel
                </Button>
                <Button type={"submit"} variant={"contained"}>
                    Submit
                </Button>
            </Box>
            <Box>
                <NotesForm name={"notes"}/>
            </Box>
        </Box>
    )
}

export default RightHalf