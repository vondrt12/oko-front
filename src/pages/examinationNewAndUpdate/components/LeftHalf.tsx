import {
    Box,
    FormControlLabel,
    FormLabel,
    GridSize,
    InputAdornment,
    MenuItem,
    Paper,
    Radio,
    RadioGroup,
    Stack,
    TextField,
    Typography,
    useTheme
} from "@mui/material";
import {ChangeEvent, FC, PropsWithChildren, ReactNode, SyntheticEvent, useEffect, useState} from "react";
import Grid from "@mui/material/Unstable_Grid2";
import {useFormContext} from "react-hook-form";
import useRegisterWrapper from "../../../hooks/useRegisterWrapper";
import CombinedTextField from "../../../components/ui/layout/CombinedTextField";
import {useGetLensesQuery} from "../../../context/store/apiSlice";

/**
 * Layout component for examination form input.
 *
 * @param name label for the input
 * @param size size for md breakpoint
 * @param children input to be wrapped
 * @component
 */
const ExamInputWrapper:FC<PropsWithChildren<{name:string, size?:GridSize}>> = ({name,size =6, children})=>{
    return(
        <Grid container>
            <Grid xs={12} md={size} sx={{display:'flex', alignItems:'center'}}>
                {name}
            </Grid>
            <Grid xs={12} md sx={{
                display:'flex',
                "& > *": {
                    flex:'1 1 auto'
                }
            }}>
                {children}
            </Grid>
        </Grid>
    )
}
/**
 * Content for left half of the examination form.
 *
 * Displays form used for creating new examination or updating existing examination
 * @component
 */
const LeftHalf = ()=>{
    const theme = useTheme();
    const {data:models=[{name:"no models available", cylinders:[{type:"no cylinders available"}]}]} = useGetLensesQuery();
    const [activeModel, setActiveModel] = useState<number>(0);
    const {register, formState, setValue, watch} = useFormContext();
    const AX1default = watch("parameters.AX1");
    const customRegister = useRegisterWrapper(register, formState);
    const numberPattern = /[+-]?([0-9]*[.])?[0-9]+/

    //Manually register some inputs and sets their default value (it is done this way because of need to fetch select field options form the server)
    //Manually sets AX2 value to be perpendicular to AX1
    useEffect( ()=>{
        register("lens.lensModel");
        register("lens.cylinderModel");
        register("side");
        setValue("lens.lensModel", models[0].name);
        setValue("lens.cylinderModel", models[0].cylinders[0].type);
        if(AX1default!==""){
            setValue("parameters.AX2", Number(AX1default) + 90);
        }
    },[AX1default, register, models, setValue])

    /**
     * Changes the active model and sets value of the lensModel and cylinderModel field
     *
     * Field values are from models selected by active model
     * @param event input change event
     */
    const handleLensModelSelected =(event:ChangeEvent<HTMLInputElement>)=>{
        const newActiveModel = Number(event.target.value)
        setActiveModel(newActiveModel);
        setValue("lens.lensModel",models[newActiveModel].name);
        setValue("lens.cylinderModel",models[newActiveModel].cylinders[0].type)
    }

    /**
     * Sets value of the cylinderModel field based on active model and event
     * @param event input change event
     */
    const handleCylinderModelSelected = (event:ChangeEvent<HTMLInputElement>)=>{
        setValue("lens.cylinderModel", models[activeModel].cylinders[Number(event.target.value)].type)
    }

    //Renders cylinder selection field
    const renderLensModelCylinders = ()=>{
        return (
            <TextField defaultValue={0} select size={"small"} onChange={handleCylinderModelSelected}>
                {
                    models[activeModel]?.cylinders.map((value, index) => {
                        return (
                            <MenuItem key={index} value={index}>
                                {value.type}
                            </MenuItem>
                        )
                    })
                }
            </TextField>
        )
    }

    //Renders lens model selection field
    const renderLensModels = ()=>{
        return (
            <TextField select defaultValue={0} onChange={handleLensModelSelected} size={"small"}>
                {models.map((value, index)=>{
                    return(
                        <MenuItem key={index} value={index}>
                            {value.name}
                        </MenuItem>
                    )
                })
                }
            </TextField>
        )
    }

    /**
     * Custom MUI TextField for exam form
     * @param name name of the field
     * @param unit unit of the parameter
     * @param step number to add or subtract from field value on arrow click
     * @param onBlur function that triggers on input blur event
     * @param startAdornment end adornment for the field
     * @component
     */
    const ExamTextField = ({name, unit, step, onBlur, startAdornment}:{name:string, unit:string, step:number, onBlur?:(e:SyntheticEvent<HTMLInputElement>)=>void, startAdornment?:ReactNode})=>{
        return (
            <TextField
                type={"number"}
                size={"small"}
                {...customRegister(name, undefined,{pattern:numberPattern, onBlur:onBlur})}
                InputProps={{
                    endAdornment:<InputAdornment position="end">{unit}</InputAdornment>,
                    startAdornment: startAdornment? <InputAdornment position={"start"}>{startAdornment}</InputAdornment> : undefined
                }}
                inputProps={{step:step}}
            />
        )
    }

    return(
        <Box sx={{display:'flex', gap:2, flexDirection:'column', paddingY:theme.spacing(2)}}>
            <Typography variant={"h4"} sx={{color:theme.palette.text.primary}}>Lens Parameters</Typography>
            <Paper sx={theme=>({padding:theme.spacing(2)})}>
                <Stack spacing={2}>
                    <ExamInputWrapper name={"Lens model"}>
                        <CombinedTextField minWidth={"10rem"}>
                            {renderLensModels()}
                            {renderLensModelCylinders()}
                        </CombinedTextField>
                    </ExamInputWrapper>
                    <ExamInputWrapper name={"Spherical equivalent"}>
                        <ExamTextField name={"lens.sphericalEquivalent"} unit={"D"} step={0.01}/>
                    </ExamInputWrapper>
                    <ExamInputWrapper name={"Originally calculated axis"}>
                        <ExamTextField name={"lens.originallyCalculatedAxis"} unit={'°'} step={0.01}/>
                    </ExamInputWrapper>
                    <ExamInputWrapper name={"Current axis"}>
                        <ExamTextField name={"lens.currentAxis"} unit={'°'} step={0.01}/>
                    </ExamInputWrapper>
                    <ExamInputWrapper name={"Decentration on X and Y axis"}>
                        <CombinedTextField>
                            <ExamTextField startAdornment={"X"} name={"lens.decentrationAxisX"} unit={"mm"} step={0.01}/>
                            <ExamTextField startAdornment={"Y"} name={"lens.decentrationAxisY"} unit={"mm"} step={0.01}/>
                        </CombinedTextField>
                    </ExamInputWrapper>
                    <ExamInputWrapper name={"Tilt 1 meridian and value"}>
                            <CombinedTextField>
                                <ExamTextField startAdornment={"M"} name={"lens.tiltFirstMeridian"} unit={'°'} step={0.01}/>
                                <ExamTextField startAdornment={"V"} name={"lens.tiltFirstValue"} unit={'°'} step={0.01}/>
                            </CombinedTextField>
                    </ExamInputWrapper>
                    <ExamInputWrapper name={"Tilt 2 meridian and value"}>
                        <CombinedTextField>
                            <ExamTextField startAdornment={"M"} name={"lens.tiltSecondMeridian"} unit={'°'} step={0.01}/>
                            <ExamTextField startAdornment={"V"} name={"lens.tiltSecondValue"} unit={'°'} step={0.01}/>
                        </CombinedTextField>
                    </ExamInputWrapper>
                </Stack>
            </Paper>
            <Typography variant={"h4"} sx={{color:theme.palette.text.primary}}>
                Eye Parameters
            </Typography>
            <Paper sx={theme=>({padding:theme.spacing(2)})}>
                <Grid container spacing={1}>
                    <Grid xs={12}>
                            <FormLabel id={"side-label"}>Side</FormLabel>
                            <RadioGroup row defaultValue={"left"}  onChange={(value)=>setValue("side", value.target.value)}>
                                <FormControlLabel value="left" control={<Radio />} label={"Left"} />
                                <FormControlLabel value="right" control={<Radio />} label={"Right"} />
                            </RadioGroup>
                    </Grid>
                    <Grid  xs={6}>
                        <ExamInputWrapper name={"AL"} size={4}>
                            <ExamTextField name={"parameters.AL"} unit={'mm'} step={0.01}/>
                        </ExamInputWrapper>
                    </Grid>
                    <Grid  xs={6}>
                        <ExamInputWrapper name={"CCT"} size={4}>
                            <ExamTextField name={"parameters.CCT"} unit={'µm'} step={1}/>
                        </ExamInputWrapper>
                    </Grid>
                    <Grid  xs={6}>
                        <ExamInputWrapper name={"WtW"} size={4}>
                            <ExamTextField name={"parameters.WtW"} unit={'mm'} step={0.01}/>
                        </ExamInputWrapper>
                    </Grid>
                    <Grid  xs={6}>
                        <ExamInputWrapper name={"pACD"} size={4}>
                            <ExamTextField name={"parameters.pACD"} unit={'mm'} step={0.01}/>
                        </ExamInputWrapper>
                    </Grid>
                    <Grid  xs={6}>
                        <ExamInputWrapper name={"K1"} size={4}>
                            <ExamTextField name={"parameters.K1"} unit={'mm'} step={0.01}/>
                        </ExamInputWrapper>
                    </Grid>
                    <Grid xs={6}>
                        <ExamInputWrapper name={"AX1"} size={4}>
                            <ExamTextField name={"parameters.AX1"} unit={'°'} step={1}/>
                        </ExamInputWrapper>
                    </Grid>
                    <Grid xs={6}>
                        <ExamInputWrapper name={"K2"} size={4}>
                            <ExamTextField name={"parameters.K2"} unit={'D'} step={0.01}/>
                        </ExamInputWrapper>
                    </Grid>
                    <Grid xs={6}>
                        <ExamInputWrapper name={"AX2"} size={4}>
                            <ExamTextField name={"parameters.AX2"} unit={'°'} step={1}/>
                        </ExamInputWrapper>
                    </Grid>
                </Grid>
            </Paper>
        </Box>

    )
}

export default LeftHalf