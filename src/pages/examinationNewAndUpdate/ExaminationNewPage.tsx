import React, {FC} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {FormProvider, useForm} from "react-hook-form";
import {setError, setSuccess} from "../../context/store/statusSlice";
import {useAppDispatch} from "../../hooks/storeHooks";
import {useAddExaminationMutation} from "../../context/store/apiSlice";
import ExaminationForm from "./ExaminationForm";
import dayjs from "dayjs";

/**
 * Page for creating new examination
 * @component
 */
const ExaminationNewPage:FC = () =>{
    const {id} = useParams();
    const navigate = useNavigate();
    const methods = useForm({mode:"all", defaultValues:{side:'left', dateCreated:dayjs().format("YYYY-MM-DD")}});
    const dispatch = useAppDispatch();
    const [addExamination] = useAddExaminationMutation();

    /**
     * Function that calls add examination mutation on form submit.
     * On success displays success alert and redirects to examination detail page of the newly created examination
     * On fail displays error alert
     * @param data form data
     */
    const onSubmit = async (data:any)=> {
        try{
            await addExamination({...data, patientId:id?id:""}).unwrap().then(payload=>{
                dispatch(setSuccess("New examination successfully created"));
                navigate(`/patients/${id}/examinations/${payload.id}`);
            })

        } catch (err){
            dispatch(setError(`Examination could not be saved`));
        }
    }

    return(
        <FormProvider {...methods}>
            <ExaminationForm onSubmit={onSubmit}/>
        </FormProvider>
    )
}

export default ExaminationNewPage