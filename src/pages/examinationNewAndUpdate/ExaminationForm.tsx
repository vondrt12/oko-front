import React, {FC} from "react";
import {Container} from "@mui/material";
import {useFormContext} from "react-hook-form";
import VerticalHalfSplit from "../../components/ui/layout/VerticalHalfSplit";
import LeftHalf from "./components/LeftHalf";
import RightHalf from "./components/RightHalf";

/**
 * Examination form containing fields from examinationData. Needs to be wrapped in FormProvider component.
 * @param onSubmit function that triggers on form submit
 * @component
 */
const ExaminationForm:FC<{onSubmit:(data:any)=>void}> = ({onSubmit})=>{
    const methods = useFormContext();
    return(
        <Container maxWidth={"xl"} sx={{display:'fex', flex:'1 1 auto', flexDirection:'column'}}
                   component={"form"}
                   onSubmit={methods.handleSubmit(onSubmit)}
        >
                <VerticalHalfSplit
                    leftHalf={<LeftHalf/>}
                    rightHalf={<RightHalf/>}
                />

        </Container>
    )
}

export default ExaminationForm