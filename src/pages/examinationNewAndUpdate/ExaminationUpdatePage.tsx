import React, {FC} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {FormProvider, useForm} from "react-hook-form";
import {useAppDispatch} from "../../hooks/storeHooks";
import {useGetExaminationQuery, useUpdateExaminationMutation} from "../../context/store/apiSlice";
import {setError, setSuccess} from "../../context/store/statusSlice";
import ExaminationForm from "./ExaminationForm";

/**
 * Page for updating existing examination
 * @component
 */
const ExaminationUpdatePage:FC = () =>{
    const {id} = useParams();
    const navigate = useNavigate();
    const {data:examinationData} = useGetExaminationQuery(id);
    const methods = useForm({mode:"all", defaultValues:examinationData});
    const dispatch = useAppDispatch();
    const [updateExamination] = useUpdateExaminationMutation();
    /**
     * Function that calls update examination mutation on form submit.
     * On success displays success alert and redirects to examination detail page.
     * On fail display error alert
     * @param data
     */
    const onSubmit = async (data:any)=> {
        try{
            await updateExamination({...data}).unwrap().then(payload=>{
                dispatch(setSuccess("Examination was successfully updated"));
                navigate(-1);
            })

        } catch (err){
            console.log(err)
            dispatch(setError(`Examination could not be updated`));
        }
    }

    return(
        <FormProvider {...methods}>
            <ExaminationForm onSubmit={onSubmit}/>
        </FormProvider>
    )
}

export default ExaminationUpdatePage