import axios from "../../context/Axios";
import React, {FC, useEffect, useState} from "react";
import {Button, Col, Container, FormControl, Row} from "react-bootstrap";
import "./Gallery.css";

interface image {
	imageUrl: string;
	imageName: string;
}

const Image: FC<image> = ({ imageUrl, imageName }) => {
	return (
		<Col>
			<img className={"gallery-image"} src={imageUrl} alt={imageName} />
			<div className={"image-text"}>
				{imageName}
			</div>
		</Col>
	);
};

const Gallery = () => {
	//const [viewed, setViewed] = useState<number>(10); //scrolling => more loading
	//const [add, setAdd] = useState<boolean>(false);	//adding images?
	//const [del, setDel] = useState<boolean>(false);	//deleting images?
	const [images, setImages] = useState<Array<image>>([]);
	const [imageNames, setImageNames] = useState<Array<string>>([]);
	const [galleryOpened, setGalleryOpened] = useState<boolean>(false);
	const [search, setSearch] = useState<string>();
	//const [galleryWidth, setWidth] = useState<string>("30%"); //for scalable gallery width
	let galleryWidth = "30%";
	const openGallery = () => {
		document.getElementById("gallery-panel")!.style.width = galleryWidth;
		setGalleryOpened(!galleryOpened);
	}
	const closeGallery = () => {
		document.getElementById("gallery-panel")!.style.width = "0%";
		setGalleryOpened(!galleryOpened);
	}

	useEffect(() => {
		axios
			.get("/gallery_image_names", {})
			.then((response) => {
				setImageNames(response.data.images);
			})
			.catch((error) => {
				console.log(error);
			});
	}, [/*add, del, viewed*/]);

	useEffect(() => {
        //let i = 0;
		setImages(Array<image>());
		for (let imageName of imageNames) {
			//i++
            axios
				.get("/gallery/" + imageName, {
					responseType: "blob",
					headers: { "Content-Type": "image/png" },
				})
				.then((response) => {
					const imageUrl = URL.createObjectURL(response.data);
					const image: image = { imageUrl, imageName };
					images.push(image);
					setImages(images);
				})
				.catch((error) => {
					console.log(error);
				});
            /*if (i > viewed) {
                break;
            }*/
		}
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [imageNames]);
	
	const handleSearchChange = (e:React.ChangeEvent<HTMLInputElement>) =>{
        setSearch(e.target?.value);
    }

	const renderImages = () => {
		return images.map((image, index) => {
			if (!search || image.imageName.toUpperCase().includes(search.toUpperCase())) {
                return (
					<Image {...image} key={index} />
                )
            }else{
                return ""
            }
		});
	};
	return (
		<Container>
			<Button className={"gallery-button"} onClick={() => {
								galleryOpened ?
								closeGallery() :
								openGallery();
							}}>
								{'\u2937'}photos
			</Button>
			<Col id={"gallery-panel"}>
				<Row>
					<Col id={"search"} className={"mt-5 mb-2 mx-5"}>
            	        <FormControl placeholder={"search for image..."} onChange={handleSearchChange}/>
            	    </Col>
				</Row>
				<Row id={"image-list-wrapper"} className={"flex-grow-1 p-10 mx-4"}>
						{ renderImages() }
				</Row>
			</Col>
		</Container>
	);
};

export default Gallery;
