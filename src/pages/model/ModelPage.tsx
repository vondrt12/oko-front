import React, {FC} from "react"
import Gallery from "./Gallery"
import Model from "./Model"
import SlidersPanel from "./SlidersPanel"
import "./ModelPage.css"

const ModelPage:FC = () =>{
    return (
        <div id={"model-page-wrapper"}>
            <div id={"gallery"}>
                <Gallery/>
            </div>
            <div id={"model"}>
                <Model/>
            </div>
            <div id={"sliders"}>
                <SlidersPanel/>
            </div>
        </div>
    )
}

export default ModelPage