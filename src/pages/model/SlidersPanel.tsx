import React, {FC} from "react"
import {Col, Container, Row} from "react-bootstrap"
import "./SlidersPanel.css"

const SlidersPanel:FC = () => {
    const renderSliders = () => {
		const sliders : string[] = ["Cornea", "Bulb", "Lens", "IOL", "Iris"];
		return sliders.map((slider,i) => {
			
			return (
				<Row key={i} className={"mb-2"}>
					<span className={"slider-name"}>
						{ slider }
					</span>
					<Col>
  						<input type="range" min="0" max="100" defaultValue={"100"} className="gallery-slider" id={slider}/>
					</Col>
					
				</Row>
				);
		});
	};
    return (
        <Container>
            <Row id={"transparency-sliders"} className={"rounded p-2 py-3"}>
				<Col>
					{ renderSliders() }
				</Col>
			</Row>
        </Container>
    )
}
export default SlidersPanel