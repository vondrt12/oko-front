import {Navigate} from "react-router-dom";
import {useEffect} from "react";
import {useAppDispatch} from "../../hooks/storeHooks";
import {logout} from "../../context/store/authSlice";

const Logout = ()=>{
    const dispatch = useAppDispatch();
    useEffect(()=>{
          dispatch(logout())
    },[dispatch])
    return(<Navigate to={"/login"} replace={true}/>)
}

export default Logout