import React, {FC, ReactNode} from "react";
import {Container, Stack, Typography, useTheme} from "@mui/material";
import Patient from "./Patient";
import {PatientOverview} from "../../../context/store/dataAPI";

/**
 * Content for left half of patient selection page
 *
 * Displays list of recently visited patients
 * @param renderPatients function that renders list of recent patients
 * @component
 */
const LeftHalf =({renderPatients}:{renderPatients:(Patient:FC<PatientOverview>)=>(ReactNode | undefined)})=>{
    const theme = useTheme();
    return (
        <Container maxWidth={"sm"} sx={{
            gap:4,
            display:'flex',
            flexDirection:'column',
            height:1,
            padding:theme.spacing(2)
        }}>
            <Typography variant={"h3"} sx={{color:theme.palette.getContrastText(theme.palette.background.default)}}>
                Recent patients
            </Typography>

            <Stack spacing={1}>
                {renderPatients(Patient)}
            </Stack>
        </Container>
    )
}

export default LeftHalf