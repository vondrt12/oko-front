import React, {FC, PropsWithChildren} from "react";
import {PatientOverview} from "../../../context/store/dataAPI";
import {Typography, useTheme} from "@mui/material";
import {useNavigate} from "react-router-dom";
import Grid from "@mui/material/Unstable_Grid2";
import {ArrowForward, Circle, Person} from "@mui/icons-material";
import ListButton from "../../../components/ui/ListButton";
import {useAppDispatch} from "../../../hooks/storeHooks";
import {addRecentPatient} from "../../../context/store/recentPatientsSlice";

/**
 * Template component for displaying Patient data
 *
 * @param name name of the patient
 * @param surname surname of the patient
 * @param examCount count of patient's examinations
 * @param id patient data id
 * @param citizenId patient citizen Id
 * @component
 */
const Patient:FC<PatientOverview> = ({name, surname, examCount, id, citizenId}) =>{
    const theme = useTheme();
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    /**
     * Wrapper component for patient information text
     * @param children information text
     * @component
     */
    const InfoText:FC<PropsWithChildren> =({children})=>{
        return(
            <Grid xs={"auto"} sx={{display:'flex', alignItems:'center'}}>
                <Typography variant={"body2"} >
                    {children}
                </Typography>
            </Grid>


        )
    }
    /**
     * Circle Divider for patient info
     * @component
     */
    const InfoTextDivider = () =>{
        return (
            <Grid xs={"auto"} padding={theme.spacing(0,1)}>
                <Circle sx={{fontSize:'5px'}}/>
            </Grid>

        )
    }
    return (
        <ListButton onClick={()=> {
            navigate(`/patients/${id}`);
            dispatch(addRecentPatient(id))
        }}>
            <Grid container>
                <Grid xs={"auto"}
                      sx={{
                          justifyContent:'center',
                          alignItems:'center',
                          display:'flex',
                          [theme.breakpoints.down("sm")]:{
                              display:'none'
                          }
                      }}
                >
                    <Person sx={{fontSize:'25px', color:theme.palette.primary.main}}/>
                </Grid>
                <Grid container xs paddingLeft={theme.spacing(2)}>
                    <Grid xs={12}>
                        <Typography variant={"subtitle1"} textAlign={"left"}>
                            {name} {surname}
                        </Typography>
                    </Grid>
                    <Grid xs={12} container>
                        <InfoText>{citizenId}</InfoText>
                        <InfoTextDivider/>
                        <InfoText>examination count: {examCount}</InfoText>
                    </Grid>
                </Grid>
                <Grid sx={{display:'flex', justifyContent:'center', alignItems:'center', [theme.breakpoints.down("sm")]:{display:'none'}}}>
                    <ArrowForward sx={{fontSize:'25px', color:theme.palette.primary.main}}/>
                </Grid>
            </Grid>
        </ListButton>
    )
}

export default Patient