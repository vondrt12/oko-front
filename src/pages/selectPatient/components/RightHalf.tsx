import React, {FC, ReactNode} from "react";
import {useNavigate} from "react-router-dom";
import {
    alpha,
    Box,
    Button,
    Container,
    Divider,
    InputBase,
    Paper,
    Stack,
    styled,
    Typography,
    useTheme
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import InnerScrollWrapper from "../../../components/ui/layout/InnerScrollWrapper";
import Grid from "@mui/material/Unstable_Grid2";
import {PatientOverview} from "../../../context/store/dataAPI";
import Patient from "./Patient";
import {Add} from "@mui/icons-material";

//--------------------------------------------SETUP------------------------------------------------------------
interface RightHalfProps{
    renderPatients:(Patient:FC<PatientOverview>)=>ReactNode,
    handleSearchChange:(e:React.ChangeEvent<HTMLInputElement>)=>void,
    search:string,
    viewed:number,
    setViewed:(i:number)=>void
}

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    borderStyle:'solid',
    borderWidth:'.5px',
    borderColor:alpha(theme.palette.secondary.dark, 0.8),
    backgroundColor: alpha(theme.palette.background.paper, 0.7),
    '&:hover': {
        backgroundColor: alpha(theme.palette.secondary.dark, 0.1),
    },
    width: '95%',
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color:theme.palette.text.primary
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: theme.palette.text.primary,
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
}));

//-------------------------------------------------------------------------------------------------------------

/**
 * Content for right half of the patient selection page
 *
 * Displays search bar and list of patients
 * @param renderPatients Function that renders the list of patients
 * @param handleSearchChange Callback function for search input
 * @param viewed number of loaded patients
 * @param setViewed function to set number of loaded patients
 * @param search value of search input
 * @component
 */
const RightHalf:FC<RightHalfProps> =({renderPatients, handleSearchChange, viewed, setViewed, search})=>{
    const navigate = useNavigate();
    const theme = useTheme();
    return (
        <Container maxWidth={"md"} sx={{padding:theme.spacing(2,0), height:1}}>

            <Paper elevation={2} sx={(theme)=>(
                {
                    height:'100%',
                    display:'flex',
                    flexDirection:'column',
                    gap:3,
                    padding:theme.spacing(3),
                    color:theme.palette.getContrastText(theme.palette.background.paper)
                })}>
                <Box sx={{display:'flex', justifyContent:'center', gap:4}}>
                    <Typography variant={"h4"} color={"primary.main"}>
                        Patients
                    </Typography>
                    <Button variant={"outlined"} color={"secondary"} onClick={()=>navigate("/patients/create")} endIcon={<Add/>}>
                        Add Patient
                    </Button>
                </Box>
                <Box sx={{display:'flex', justifyContent:'center'}}>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Search…"
                            inputProps={{ 'aria-label': 'search' }}
                            value={search}
                            onChange={handleSearchChange}
                        />
                    </Search>
                </Box>
                <InnerScrollWrapper minHeight={"250px"}>
                    <Stack spacing={1} sx={{padding:theme.spacing(1)}} divider={<Divider/>}>
                        {renderPatients(Patient)}
                        <Button variant={"outlined"} color={"secondary"} onClick={()=>{setViewed(viewed+20)}}>
                            load more
                        </Button>
                    </Stack>
                </InnerScrollWrapper>
                <Grid container justifyContent={"right"}>
                    <Button variant={"contained"} color={"secondary"} onClick={()=>navigate("/patients/create")}>
                        New Patient
                    </Button>
                </Grid>
            </Paper>
        </Container>
    )
}

export default RightHalf