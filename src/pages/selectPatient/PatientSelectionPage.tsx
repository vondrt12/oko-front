import React, {FC, useEffect, useState} from "react";
import {PatientOverview} from "../../context/store/dataAPI";
import {Box, CircularProgress, Container} from "@mui/material";
import VerticalHalfSplit from "../../components/ui/layout/VerticalHalfSplit";
import LeftHalf from "./components/LeftHalf";
import RightHalf from "./components/RightHalf";
import { useGetPatientsQuery} from "../../context/store/apiSlice";
import {useAppDispatch, useAppSelector} from "../../hooks/storeHooks";
import {setError, setSuccess} from "../../context/store/statusSlice";
import {selectRecentPatients} from "../../context/store/recentPatientsSlice";

//-------------------------------------------------PAGE COMPONENT-------------------------------------------------------
/**
 * Page for searching and selecting patient
 * @component
 */
const PatientSelectionPage:FC = () => {
    //--------------setup-----------------------------
    //number of results showed in UI
    const [viewed, setViewed] = useState<number>(20);
    const [search, setSearch] = useState<string>("");
    const dispatch = useAppDispatch();
    const {data:patients, isFetching,} = useGetPatientsQuery(viewed);
    const recentPatients = useAppSelector(selectRecentPatients);

    useEffect(()=>{
        if(!isFetching && patients){
            dispatch(setSuccess("More patients were successfully loaded"));
        }
        if(!isFetching && patients && patients.length < viewed){
            dispatch(setError("All patient profiles are already loaded"));
        }
    },[isFetching, patients, viewed, dispatch])
    //------------------------------------------------

    //--------------Methods-----------------------------
    /**
     * Handles search input change
     * @param e input event
     */
    const handleSearchChange = (e:React.ChangeEvent<HTMLInputElement>) =>{
        setSearch(e.target?.value);
    }

    /**
     * Function that renders patients
     *
     * excludes patients that does not match search
     * @param Patient template component for patient
     */
    const renderPatients = (Patient:FC<PatientOverview>)=>{
        let empty:boolean = true;
        const searchResult = patients?.map((patient, index)=> {
            const searchValue = search.replace(/\s/g, "").toUpperCase()
            if (!patient.name || !patient.surname || !patient.citizenId || search === ""
                || patient.name.toUpperCase().concat(patient.surname.toUpperCase()).includes(searchValue)
                || patient.citizenId.toUpperCase().includes(search.toUpperCase())
            ) {
                empty = false;
                return (
                    <Patient {...patient} key={index}/>
                )
            }else{
                return ""
            }
        })
        if(isFetching) return <Box sx={{display:"flex", justifyContent:"center"}}><CircularProgress/></Box>
        if(empty) return <Box sx={{display:"flex", justifyContent:"center"}}>No profiles matched with current search input</Box>
        return searchResult
    }

    /**
     * Function that renders patients whose detail page was recently visited
     * @param Patient template component for patient
     */
    const renderRecentPatients = (Patient:FC<PatientOverview>)=>{
        return patients?.filter((value)=>recentPatients.includes(value.id)).map((patient, index)=> {
                return (
                    <Patient {...patient} key={index}/>
                )

        })

    }
    //------------------------------------------------

    //--------------Render-----------------------------
    return (
        <Container maxWidth={"lg"} sx={{display:'flex', flex:'1 1 auto', flexDirection:'column'}}>
            <VerticalHalfSplit
                leftHalf={<LeftHalf renderPatients={renderRecentPatients}/>}
                rightHalf={<RightHalf renderPatients={renderPatients}
                                      viewed={viewed} setViewed={setViewed}
                                      handleSearchChange={handleSearchChange}
                                      search={search}
                />}

            />
        </Container>


    )
    //------------------------------------------------
}

export default PatientSelectionPage