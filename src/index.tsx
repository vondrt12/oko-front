import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './modules/StringToSnakeCase'
import {Provider} from "react-redux";
import store from "./context/store/Store";
import {Experimental_CssVarsProvider as CSSVarsProvider} from "@mui/material/styles";
import theme from "./context/themes/DarkTheme";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {LocalizationProvider} from "@mui/x-date-pickers";
import {GoogleOAuthProvider} from "@react-oauth/google";


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
      <Provider store={store}>
          {/*@ts-ignore*/}
          <GoogleOAuthProvider clientId={window._env_.CLIENT_ID ? window._env_.CLIENT_ID : ""}>
              <CSSVarsProvider defaultMode={"dark"} defaultColorScheme={{light:"light", dark:"dark"}} storageWindow={window} theme={theme}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <App />
                  </LocalizationProvider>
              </CSSVarsProvider>
          </GoogleOAuthProvider>
      </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
