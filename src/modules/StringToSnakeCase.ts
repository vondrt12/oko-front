// eslint-disable-next-line no-extend-native
String.prototype.toSnakeCase = function(this: string){
    return this.toLowerCase().replace(" ","_");
}

export {}