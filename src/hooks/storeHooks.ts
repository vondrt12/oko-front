import {AppDispatch, RootState} from "../context/store/Store";
import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";

type DispatchFunc = () => AppDispatch;
export const useAppDispatch: DispatchFunc = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;