import {
    FieldError,
    FieldErrorsImpl,
    FieldPath,
    FieldValues,
    FormState,
    Merge,
    RegisterOptions,
    UseFormRegister,
    UseFormRegisterReturn
} from "react-hook-form";

type RegisterWrapperReturn <TFieldName extends FieldPath<FieldValues> = FieldPath<FieldValues>> = UseFormRegisterReturn<TFieldName> & {
    error:boolean,
    helperText:string
}
export type HelperTextReducer = (error: FieldError | Merge<FieldError, FieldErrorsImpl<any>> | undefined)=>string
type CustomRegister<TFieldValues extends FieldValues> = <TFieldName extends FieldPath<TFieldValues> = FieldPath<TFieldValues>>(name:TFieldName,  helperTextReducer?:HelperTextReducer, options?: RegisterOptions<TFieldValues, TFieldName>)=>RegisterWrapperReturn

/**
 * Custom react hook that wraps react-hook-form register function into function that on top of registerReturn returns some props for TextField
 *
 * Wrapper function returns:
 *                  error - boolean that indicates if field is invalid
 *                  helperText - Text to display if field is in invalid state
 * @param {UseFormRegister} register register function returned by useForm hook from react-hook-form
 * @param {FormState} formState formState dictionary returned by useForm hook from react-hook-form
 * @return {CustomRegister} function that is used for registering MUI TextField
 * @category hooks
 */
function useRegisterWrapper<TFieldValues extends FieldValues = FieldValues>(register:UseFormRegister<TFieldValues>, formState:FormState<TFieldValues>): CustomRegister<TFieldValues>{
    /**
     * Custom register function
     *
     * @param name name of the field (react-hook-form register name)
     * @param {HelperTextReducer} [helperTextReducer] reducer that should return helperText based on type of error set in formState.errors, if not set default reducer will be used
     * @param {RegisterOptions} options react-hook-form register options
     */
    return (name, helperTextReducer, options) => {
        const {errors} = formState
        const registerReturn = register(name, options);
        if(!helperTextReducer){
            helperTextReducer = (errors)=>{
                switch (errors?.type){
                    case "pattern": return "input has wrong format";
                    case "required": return "this field is required";
                    default: return ""
                }
            }
        }
        return {
            error: formState.errors.hasOwnProperty(registerReturn.name),
            helperText: formState.errors.hasOwnProperty(registerReturn.name) ?
                helperTextReducer(errors[registerReturn.name])
                :
                "",
            ...registerReturn
        }
    }
}

export default useRegisterWrapper