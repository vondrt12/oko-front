import React, {FC, useEffect, useState} from 'react'
import './NotesForm.css'
import {convertFromRaw, convertToRaw, Editor, EditorState} from "draft-js";
import {useController} from "react-hook-form";
import {Stack} from "@mui/material";
import Toolbar from "./Toolbar";

/** Form field for creating rich text
 *
 * Support Bold, Italic, Underlined, Bullet list, Ordered list
 * Initial text of editor can be set with useForm hook as default value of field with name matching @param 'name'
 * @param {string} name name representing the notesForm as field in parent form
 * @category component-forms
 */
const NotesForm: FC<{name:string}> = ({name}) => {
    const {field, formState} = useController({name});
    let editorStartState = EditorState.createEmpty();
    //holds draftJS editor state
    const [editorState, setEditorState] = useState(editorStartState);

    useEffect(()=>{
        if(formState.defaultValues && formState.defaultValues[name]){
            console.log("setup")
            const content = convertFromRaw(formState.defaultValues[name]);
            setEditorState(EditorState.createWithContent(content));
        }
    },[formState.defaultValues, name])

    /**
     * handles onChange callback of the draftJS editor
     * @param eState editor state
     */
    const handleEditorStateChange = (eState:EditorState) => {
        //changes editor state
        setEditorState(eState);
        //changes value in the form
        const raw = convertToRaw(eState.getCurrentContent());
        field.onChange(raw);
    }
    return(

            <Stack sx={{width:1}} spacing={2}>
                <Toolbar editorState={editorState} handleEditorStateChange={handleEditorStateChange}/>
                <Editor editorState={editorState} onChange={handleEditorStateChange}/>
            </Stack>
    )
}

export default NotesForm