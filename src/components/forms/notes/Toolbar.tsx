import React, {FC, useEffect, useState} from "react";
import {EditorState, RichUtils} from "draft-js";
import {Stack, ToggleButton, ToggleButtonGroup} from "@mui/material";
import {FormatBold, FormatItalic, FormatListBulleted, FormatListNumbered, FormatUnderlined} from "@mui/icons-material";

/**
 * Toolbar for NotesForm
 * @param {EditorState} editorState state of draftJS editor
 * @param {React.Dispatch<React.SetStateAction<EditorState>>} setEditorState callback function for changing state of the editor
 * @category component-forms
 */
const Toolbar:FC<{editorState:EditorState, handleEditorStateChange:(eState:EditorState)=>void}>
    = ({editorState, handleEditorStateChange}) =>
{

    const [formats, setFormats] = useState<Array<string>>([])
    const [list, setList] = useState<string>("")

    /**
     * Activates and deactivates font formats (Bold, Italic, Underline)
     * @param event action event
     * @param newFormats array with active formats
     */
    const handleFormat = ( event:React.MouseEvent<HTMLElement>, newFormats:Array<string>) =>{

        newFormats.filter(x=>!formats.includes(x))
            .concat(formats.filter(x=>!newFormats.includes(x))).forEach((value)=>{
            handleEditorStateChange(RichUtils.toggleInlineStyle(editorState, value));
        })
        setFormats(newFormats);
    }

    /**
     * Activates and deactivates list format (Bullet, Ordered, None)
     * @param event action event
     * @param newList active list format
     */
    const handleList = (event:React.MouseEvent<HTMLElement>, newList:string)=>{
        handleEditorStateChange(RichUtils.toggleBlockType(editorState, list));
        handleEditorStateChange(RichUtils.toggleBlockType(editorState, newList));
        setList(newList);
    }

    //updates toggle state of the tool buttons when user changes cursor position inside the editor
    useEffect(()=>{
        setFormats(editorState.getCurrentInlineStyle().toArray())
        setList(RichUtils.getCurrentBlockType(editorState))
    },[editorState])
    return (

            <Stack direction={"row"} width={1} spacing={2}>
                <ToggleButtonGroup
                    value={formats}
                    onChange={handleFormat}
                    aria-label="text formatting"
                    color={"primary"}
                >
                    <ToggleButton value="BOLD" aria-label="bold">
                        <FormatBold/>
                    </ToggleButton>
                    <ToggleButton value="ITALIC" aria-label="italic">
                        <FormatItalic/>
                    </ToggleButton>
                    <ToggleButton value="UNDERLINE" aria-label="underlined">
                        <FormatUnderlined/>
                    </ToggleButton>
                </ToggleButtonGroup>
                <ToggleButtonGroup exclusive value={list} onChange={handleList} color={"primary"}>
                    <ToggleButton value={"unordered-list-item"} >
                        <FormatListBulleted/>
                    </ToggleButton>
                    <ToggleButton value={"ordered-list-item"}>
                        <FormatListNumbered/>
                    </ToggleButton>
                </ToggleButtonGroup>

            </Stack>
    )
}

export default Toolbar