import {Alert, Snackbar} from "@mui/material";
import {useSelector} from "react-redux";
import {selectStatus, setIdle} from "../../context/store/statusSlice";
import {useAppDispatch} from "../../hooks/storeHooks";

/**
 * Displays alerts in top right corner of the screen. Alerts are Triggered by dispatching setError or setSuccess action.
 * @category component-ui
 */
const StoreAlerts = ()=>{
    const status = useSelector(selectStatus);
    const dispatch = useAppDispatch();
    const handleClose = ()=>{
        dispatch(setIdle())
    }
    return(
        <Snackbar open={status.state!=="idle"} autoHideDuration={10000} onClose={handleClose} anchorOrigin={{vertical:"top",horizontal:"right"}}>
            <Alert onClose={handleClose} severity={status.state==="idle" ? undefined : status.state}>
                {status.message}
            </Alert>
        </Snackbar>
    )
}

export default StoreAlerts