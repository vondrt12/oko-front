import React, {FC, PropsWithChildren} from "react";
import {Button} from "@mui/material";

/**
 * Styled MUI Button for displaying clickable list items
 * @param {React.MouseEventHandler<HTMLAnchorElement>} onClick Callback function that triggers on click
 * @param {ReactNode} [children] Content of the button
 * @category component-ui
 */
const ListButton:FC<PropsWithChildren<{onClick?: React.MouseEventHandler<HTMLAnchorElement>}>> = ({onClick, children})=> {

    return(
        <Button variant={"text"}
            sx={(theme)=>({
                color:theme.palette.text.primary,
                display:'block',
                borderRadius:'5px',
            })}
           onClick={onClick}
            href={""}
        >
            {children}
        </Button>
    )
}

export default ListButton