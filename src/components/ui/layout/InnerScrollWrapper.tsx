import React, {FC, PropsWithChildren} from "react";
import {Box} from "@mui/material";

/**
 * Wrapper that makes inner content scrollable if it is longer than the height of the wrapper. Wrapper fills out height of its container.
 *
 * For proper functionality its container should have specified height and have 'display:flex' set.
 * @param {string} [minHeight] minimal height of scroll wrapper
 * @param {ReactNode} [children] inner content of the scroll wrapper
 * @category component-layout
 */
const InnerScrollWrapper: FC<PropsWithChildren<{minHeight?:string}>> = ({minHeight="", children})=>{
    return(
        <Box sx={
            (theme)=>(
                {
                    minHeight:minHeight,
                    height:"0px",
                    overflow:"auto",
                    flex:'1 1 auto!important'
                }
                )
        }>
                {children}
        </Box>
    )
}

export default InnerScrollWrapper