import React, {FC, PropsWithChildren} from "react";
import PageNavbar from "./Navbar";
import {useMediaQuery, useTheme} from "@mui/material";

/**
 * Router component that renders Page navigation at top or bottom of the page based on the theme breakpoint.
 * Navigation renders on top when width of browser is greater than md breakpoint and on bottom otherwise.
 * @param {ReactNode} [children] Content to render with the navigation
 * @category component-layout
 */
const ShowNavbar:FC<PropsWithChildren> = ({children}) =>{
    const theme = useTheme();
    const greaterThanMid = useMediaQuery(theme.breakpoints.up("md"));
    return(
        <>
            {!greaterThanMid && children}
            <PageNavbar/>
            {greaterThanMid && children}
        </>
    )
}

export default ShowNavbar