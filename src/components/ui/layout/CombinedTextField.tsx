import {FC, PropsWithChildren} from "react";
import {Box, useTheme} from "@mui/material";

/**
 * Props for CombinedTextField component
 */
interface CombinedTextFieldProps{
    /**
     * Max width of the field
     */
    maxWidth?:string,

    /**
     * Minimal width of the field
     */
    minWidth?:string
}

/**
 * Simple wrapper for MUI TextField components that clamps them together and straightens inner corners
 * @param {ReactNode} [children] TextField inputs to be clamped together
 * @param {string} [maxWidth] Max width of combined Input
 * @param {string} [minWidth] Min width of combined Input
 * @category component-layout
 */
const CombinedTextField:FC<PropsWithChildren<CombinedTextFieldProps>> = ({children, maxWidth, minWidth})=>{
    const theme = useTheme();
    return(
        <Box sx={{
            maxWidth:maxWidth,
            minWidth:minWidth,
            display:'flex',
            flexDirection:'row',
            "& > .MuiFormControl-root":{
                flex:"1 1 auto"
            },
            "& > .MuiFormControl-root:first-of-type":{
                "& > .MuiInputBase-root":{
                    borderRadius:`${theme.shape.borderRadius}px 0px 0px ${theme.shape.borderRadius}px`
                }
            },
            "& > .MuiFormControl-root:last-of-type":{
                "& > .MuiInputBase-root":{
                    borderRadius:`0px ${theme.shape.borderRadius}px ${theme.shape.borderRadius}px 0px`
                }
            }
        }}>
            {children}
        </Box>
    )
}

export default CombinedTextField