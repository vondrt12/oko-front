import {CSSProperties, FC, ReactNode} from "react";
import {Col, Container, Row} from "react-bootstrap";

interface VerticalHalfSplitProps{
    leftHalf?: ReactNode,
    leftHalfStyle?:CSSProperties,

    rightHalfStyle?:CSSProperties,
    rightHalf?: ReactNode
}

/**
 * Layout component that splits its container into two halves. When width of the browser is less than large breakpoint the two halves will be displayed in a column on top of themselves.
 * @param {ReactNode} [leftHalf] Content of left half of the container
 * @param {ReactNode} [rightHalf] Content of right half of the container
 * @param {CSSProperties} [leftHalfStyle] Styling of left half container
 * @param {CSSProperties} [rightHalfStyle] Styling of right half container
 * @category component-layout
 */
const VerticalHalfSplit:FC<VerticalHalfSplitProps> = ({leftHalf, rightHalf, leftHalfStyle, rightHalfStyle})=>{
    return (
        <Container fluid className={"flex-fill d-flex"}>
            <Row className={"flex-fill p-0"}>

                <Col lg={6} className={"p-0"} style={leftHalfStyle}>
                    {leftHalf}
                </Col>
                <Col lg={6} className={"p-0"} style={rightHalfStyle}>
                    {rightHalf}
                </Col>


            </Row>
        </Container>

    )
}

export default VerticalHalfSplit