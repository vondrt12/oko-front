import {Box, ButtonProps, IconButton, Tooltip, Typography} from "@mui/material";
import React, {FC} from "react";
import {Delete} from "@mui/icons-material";

/**
 * Button with trash bin icon and title on the left of the button
 *
 * @param {string} tooltip Text to show after hovering over the button
 * @param {string} [label] Text to show on the left of the button
 * @param {ButtonProps} buttonProps MUI button props
 * @category component-ui
 */
const RemoveButton:FC<ButtonProps & {tooltip:string, label?:string}> = ({tooltip, label,...buttonProps})=>{
    return(
        <Box justifyContent={"right"} display={"flex"} alignItems={"center"}>
            {label &&
                <Typography variant={"h5"} align={"center"} sx={{display:'flex', alignItems:'center'}}>
                    {label}
                </Typography>
            }
            <Tooltip title={tooltip}>
                <IconButton size={"medium"} aria-label={"Edit"} color={"error"} {...buttonProps} sx={{width:'40px', height:'40px'}}>
                    <Delete/>
                </IconButton>
            </Tooltip>
        </Box>
    )
}

export default RemoveButton