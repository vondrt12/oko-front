import {FC} from "react";
import {Paper} from "@mui/material";
import InnerScrollWrapper from "./layout/InnerScrollWrapper";
import {convertFromRaw, Editor, EditorState, RawDraftContentState} from "draft-js";

/**
 * Styled paper for displaying draftJS Raw content using draft js Editor in readonly setting
 *
 * @param {RawDraftContentState} [content] RawDraftContentState representing the content of the notes.
 * @category component-ui
 */
const NotesPaper:FC<{content?:RawDraftContentState}> = ({content})=>{
    let editorState:EditorState = EditorState.createEmpty()
    if(content) {
        const contentState = convertFromRaw(content);
       editorState = EditorState.createWithContent(contentState);
    }

    return (
        <Paper elevation={2} sx={(theme)=>({
            display:'flex',
            padding:theme.spacing(2),
            backgroundColor:theme.palette.notes.main,
            color:theme.palette.getContrastText(theme.palette.notes.main),
            flex:'1 1 auto',
            flexDirection:'column'
        })}>
            <InnerScrollWrapper minHeight={"200px"}>
                <Editor editorState={editorState} readOnly={true} onChange={()=>{}}/>
            </InnerScrollWrapper>
        </Paper>
    )
}

export default NotesPaper