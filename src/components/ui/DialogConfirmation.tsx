import React, {FC, ReactNode, useState} from "react";
import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField
} from "@mui/material";

interface DialogConfirmationProps{
    handleCloseOk: ()=>void,
    handleCloseCancel: ()=>void,
    open:boolean,
    text?:ReactNode,
    title:ReactNode,
    textConfirmation?:string
}

/**
 * Pop up dialog with two options to accept or decline. Can also make user to fill in confirmation text before they can click accept.
 *
 * @param {function():void} handleCloseOk callback function on accepting
 * @param {function():void} handleCloseCancel callback function on declining
 * @param {boolean} open boolean representing whether is the dialog opened or closed
 * @param {ReactNode} [text] Inner text of the dialog
 * @param {ReactNode} title Title of the dialog
 * @param {string} [textConfirmation] If set, user has to fill in input with text that matches this parameter before they can click accept
 * @category component-ui
 */
const DialogConfirmation:FC<DialogConfirmationProps> = ({handleCloseOk, handleCloseCancel, open,text, title, textConfirmation})=>{
    const [userInput, setUserInput] = useState<string | undefined>(textConfirmation? "" : undefined)
    return (
        <Dialog
            open={open}
            onClose={handleCloseCancel}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                {title}
            </DialogTitle>
            <DialogContent>
                {text}
            </DialogContent>
            <DialogActions sx={{justifyContent:"center"}}>
                {textConfirmation && <TextField size={"small"} value={userInput} onChange={(value)=>{setUserInput(value.target.value)}}/>}
                <Box sx={{display:"flex", gap:1}}>
                    <Button onClick={handleCloseCancel} variant={"outlined"} color={"secondary"}>Cancel</Button>
                    <Button onClick={handleCloseOk} autoFocus variant={"contained"} disabled={textConfirmation!==userInput}>
                        OK
                    </Button>
                </Box>

            </DialogActions>
        </Dialog>
    )
}

export default DialogConfirmation