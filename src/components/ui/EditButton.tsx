import React, {FC} from "react";
import {Box, IconButton, Tooltip, Typography} from "@mui/material";
import {Edit} from "@mui/icons-material";

/**
 * Button with pencil icon and label on the left
 * @param {React.MouseEventHandler<HTMLAnchorElement>} [onClick] callback function on button clicked
 * @param {string} [label] Text that displays on the left side of the button
 * @param {string} [tooltip] Text that appears when hovering over the button
 * @category component-ui
 */
const EditButton:FC<{onClick?:React.MouseEventHandler<HTMLAnchorElement>, label?:string, tooltip?:string}> = ({onClick, label, tooltip})=>{
    return (
        <Box justifyContent={"right"} display={"flex"}>
            {label &&
                <Typography variant={"h5"} align={"center"} sx={{display:'flex', alignItems:'center'}}>
                    {label}
                </Typography>
            }
            <Tooltip title={tooltip}>
                <IconButton size={"medium"} onClick={onClick} href={""} aria-label={"Edit"} color={"secondary"}>
                    <Edit/>
                </IconButton>
            </Tooltip>
        </Box>
    )
}

export default EditButton