import React, {FC} from "react"
import {Navigate, Outlet} from "react-router-dom";
import {useAppSelector} from "../../hooks/storeHooks";
/**
 * Router component that redirects you to login page if client is not logged in
 * @category component-auth
 */
const RequireAuth:FC = ()=>{
    const userToken = useAppSelector((state)=>state.auth.userToken)
    return(
        userToken ?
            <Outlet/>
            :
            <Navigate to={"/login"}/>
    )
}

export default RequireAuth