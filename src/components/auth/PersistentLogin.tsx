import {useAppDispatch} from "../../hooks/storeHooks";
import {useGetUserQuery} from "../../context/store/apiSlice";
import {useEffect} from "react";
import {setCredentials} from "../../context/store/authSlice";
import {Outlet} from "react-router-dom";
/**
 * This component will fetch user data on render if userToken is available and refetches data every 15 minutes
 * @category component-auth
 */
const PersistentLogin = ()=>{
    const dispatch = useAppDispatch();
    const {data} = useGetUserQuery(undefined, {pollingInterval:90000});

    useEffect(()=>{
        if(data) dispatch(setCredentials(data))
    },[data, dispatch])
    return(
        <Outlet/>
    )
}

export default PersistentLogin